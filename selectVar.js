const product = {
  productId: "B087YYGSBY",
  title: "Under Armour Men's Charged Assert 9 Running Shoe 10.5 Black/White",
  variantOptions: ["Size", "Color"],
  description:
    " Lightweight mesh upper with 3-color digital print delivers complete breathability. Durable leather overlays for stability & that locks in your midfoot. EVA sockliner provides soft, step-in comfort. Charged Cushioning® midsole uses compression molded foam for ultimate responsiveness & durability. Solid rubber outsole covers high impact zones for greater durability with less weight.",
  merchant: "amazon",
  rating: 4.6,
  in_stock: true,
  baseCurrency: "USD",
  price: {
    value: 3478840,
    currency: "NGN",
  },
  oldPrice: null,
  doVariantsHaveProductPage: true,
  variants: [
    {
      productId: "B08MV8SPPQ",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B087YXNGSL",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Black/White",
      },
    },
    {
      productId: "B09693KLGF",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08PZ8N7Y8",
      images: [
        "https://m.media-amazon.com/images/I/711p69IpvJL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/41I63a32FSL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61n3kBOSnRL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/41biW4AQH6L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51HlL0ZVtwL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/414egjgn-uL._AC_UL1000_.jpg",
      ],
      price: null,
      criteria: {
        Size: "14",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MVBG6M3",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08MV9GXH5",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B091KFXJL1",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09692BF12",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B09694PQ69",
      images: [
        "https://m.media-amazon.com/images/I/81a85bwBwdL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61OEuU4x+KL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/617bllX+2rL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/51jWro0KuFL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/71uj1Muxl+L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61w+d2m5bvL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61InryzaXIL._AC_UL1000_.jpg",
      ],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08CFMDCPQ",
      images: [],
      price: null,
      criteria: {
        Size: "7 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MV9SQVZ",
      images: [],
      price: null,
      criteria: {
        Size: "8 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CF6HP8T",
      images: [],
      price: null,
      criteria: {
        Size: "9.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MV938DS",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B096942XMM",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08MVB73WK",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08CF4DNGM",
      images: [],
      price: null,
      criteria: {
        Size: "15 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08CFVFTV5",
      images: [],
      price: null,
      criteria: {
        Size: "12.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MVBTL27",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B087TP5C6R",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09691S9C7",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09LS81RJH",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B096942NYS",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B08CFT73VN",
      images: [],
      price: null,
      criteria: {
        Size: "12 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B09LS7HVK1",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B087YYGSBY",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MVBBL7R",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B087YSHL9G",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B087T7F5YF",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09693VYP3",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08CFTG6KV",
      images: [],
      price: null,
      criteria: {
        Size: "9.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MVC6649",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B09693FQP6",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08CF4CB1V",
      images: [],
      price: null,
      criteria: {
        Size: "14 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08CFPWS6K",
      images: [],
      price: null,
      criteria: {
        Size: "12.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B087NQHVBV",
      images: [
        "https://m.media-amazon.com/images/I/81W+D3QH0fS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71cwftYrgtS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61FS96PZAaS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71fYyvJDPUS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/818n42upN7S._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71mxAEx3l4S._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/810xrD5usFS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71tRyK+4PxS._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "8",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B096944H9P",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B09LS7MNM1",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08CF4J4F2",
      images: [],
      price: null,
      criteria: {
        Size: "15 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CFTPBHD",
      images: [],
      price: null,
      criteria: {
        Size: "8 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B087Z4LTNY",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MV8YNYM",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B09692WWH2",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09693FRDM",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09692K2R1",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B087YWM8RX",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Black/White",
      },
    },
    {
      productId: "B087TD436L",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MV9NCY4",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B087TL5T75",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B0969396CY",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B091KHFMBB",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B09693ZGRR",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B09692T5LL",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CF5Y5M2",
      images: [],
      price: null,
      criteria: {
        Size: "11.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MVB72SN",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B08MV8FKN8",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B08CFRXGQK",
      images: [],
      price: null,
      criteria: {
        Size: "10 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MV9HGRN",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B08CG1FPQL",
      images: [],
      price: null,
      criteria: {
        Size: "10.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MVC5VTD",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B0968ZT77T",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CFVPY2M",
      images: [],
      price: null,
      criteria: {
        Size: "8 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CFT75X3",
      images: [],
      price: null,
      criteria: {
        Size: "10 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B09694MYPF",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B08MVD677K",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B09692D2WF",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B08MV95VHY",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08MV9Q7N1",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08CFTTZJB",
      images: [],
      price: null,
      criteria: {
        Size: "10.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MV9ZH3Y",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B09692LGB8",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08MV9SRMD",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B09693949B",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08CFTQD7J",
      images: [],
      price: null,
      criteria: {
        Size: "14 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08CFSGYRV",
      images: [],
      price: null,
      criteria: {
        Size: "10.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CF4Q8S3",
      images: [],
      price: null,
      criteria: {
        Size: "13 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MVBGJQ9",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B08MVBCVTN",
      images: [],
      price: null,
      criteria: {
        Size: "7 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CF5FSMJ",
      images: [],
      price: null,
      criteria: {
        Size: "10 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08CFVTGLT",
      images: [],
      price: null,
      criteria: {
        Size: "9 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08CF54NWX",
      images: [],
      price: null,
      criteria: {
        Size: "14 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CF5QNT9",
      images: [],
      price: null,
      criteria: {
        Size: "7 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B09692STW9",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B08MV9Q7NM",
      images: [],
      price: null,
      criteria: {
        Size: "12 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08MV8TSHN",
      images: [],
      price: null,
      criteria: {
        Size: "14 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08MVBJRH1",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B08CFLC1QG",
      images: [],
      price: null,
      criteria: {
        Size: "13 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CFSGMYN",
      images: [],
      price: null,
      criteria: {
        Size: "8.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B087NQPGB9",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B096928NL8",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08MV7N7LB",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B08CFSKG1L",
      images: [],
      price: null,
      criteria: {
        Size: "8.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MVBC7H9",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B08MVF7L2L",
      images: [
        "https://m.media-amazon.com/images/I/517w5lVaUjL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/518HbmAoFiL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61WRD7STRHS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/51Hz1vnouGL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51j85mDwQSL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61abP8Kfi9L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/81GOaXclxWS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/81q6BiFQ5-S._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08MV9XD2N",
      images: [],
      price: null,
      criteria: {
        Size: "9 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B09LS7Z3TD",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B09693QD3K",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B091KGJBSX",
      images: [
        "https://m.media-amazon.com/images/I/51N9LYNfAVL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51wufijz52L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61aW5UhbGaL._AC_UL1250_.jpg",
        "https://m.media-amazon.com/images/I/51HtDQbZ7TL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51oe+YL+l0L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/6150RYpLuGL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51T2z-7XfML._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/81yvPSKIixL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/616ut8Jn21L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51MrO4EM2lL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/81yvPSKIixL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71gLOcsEHPS._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "14",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MV6Z4RC",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B09LS8RFFB",
      images: [
        "https://m.media-amazon.com/images/I/71qQGvjxSyL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61ajopM4AIL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61k3ZTyYhGL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71D-x8vtsuL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/719MbybAMxL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71pN+a+-84L._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71ZOcqkrL4L._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71Xp5AT1hlL._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "12",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MV938FL",
      images: [
        "https://m.media-amazon.com/images/I/51ZVXNT+iuL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51AR+YmeHOL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/41PiaWtIRUL._AC_.jpg",
        "https://m.media-amazon.com/images/I/41JIdBtPyvL._AC_.jpg",
      ],
      price: null,
      criteria: {
        Size: "10.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CF5NVZD",
      images: [],
      price: null,
      criteria: {
        Size: "15 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B087NR2DKH",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08CFTQ974",
      images: [],
      price: null,
      criteria: {
        Size: "11 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV95C63",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B08CFZ4TVG",
      images: [],
      price: null,
      criteria: {
        Size: "7.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B087TH2Y1D",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08J2VDDTK",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B09691W49F",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B087TC4NBL",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CFVR1YR",
      images: [],
      price: null,
      criteria: {
        Size: "7 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B09691JJL7",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B087TH2Y1F",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08CFPWB73",
      images: [],
      price: null,
      criteria: {
        Size: "8.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MV8ZC4K",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B087TH4JM6",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B087TGFLYQ",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B087NQ1R4G",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B09691CJQL",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B09694F1S2",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B09LS6CVLG",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B087TC5YWM",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B091KPVNTY",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MVB6587",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B09LS87FFR",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MVDLZGG",
      images: [
        "https://m.media-amazon.com/images/I/51mNiP1dRyL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51rwcp+w9yL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61ugjjRADbS._AC_UL1471_.jpg",
        "https://m.media-amazon.com/images/I/51C5BkRM00L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51P0+w0do2L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51WTiS6Aj7L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/71QoO-JstKS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71aMf-bzoDS._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "14",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B09693PCTP",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B087TJ7M17",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MV5JSZZ",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B095LQFJW9",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B087NQPM27",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08CFWCCXN",
      images: [
        "https://m.media-amazon.com/images/I/516U3044+LL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51vqQapDyLL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61zk0h6cB8S._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/51r3tUzUCXL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61ZJuHwPUzL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61mhNmhBlCL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/71n4uMr-qxS._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71dfCDzMFgS._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "7 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08CF4HX7P",
      images: [],
      price: null,
      criteria: {
        Size: "12 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CF5KS9N",
      images: [],
      price: null,
      criteria: {
        Size: "11.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09691FSBS",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CFSK9K8",
      images: [],
      price: null,
      criteria: {
        Size: "9.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B0969343BQ",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B096928TBH",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B087T7D7KS",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV94G6N",
      images: [],
      price: null,
      criteria: {
        Size: "7.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B087NQCL76",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B09692XCT6",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08CF524W8",
      images: [],
      price: null,
      criteria: {
        Size: "8.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08CF58LV4",
      images: [],
      price: null,
      criteria: {
        Size: "11 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B087YYNBFB",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B087TMGX3N",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV9XB36",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B087YQCWKG",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CF53W7C",
      images: [],
      price: null,
      criteria: {
        Size: "10 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MVC623N",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B096935PH3",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B0969188Y7",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B091KHC51W",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CFT7KM9",
      images: [],
      price: null,
      criteria: {
        Size: "11 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08MV9WZ4L",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B09691VV9X",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B08MVCNP83",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B09693P7G4",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B09693CD6N",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08CF2ZW7G",
      images: [],
      price: null,
      criteria: {
        Size: "12 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08CFQLV96",
      images: [],
      price: null,
      criteria: {
        Size: "9 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09693P7G2",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09694H8CS",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B087NQMFLP",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08MVBFT3J",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B087TN7X75",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B08CFRNXK9",
      images: [],
      price: null,
      criteria: {
        Size: "12.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MV8YKJW",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08CG1H38K",
      images: [
        "https://m.media-amazon.com/images/I/410-L0vF3+L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51hlA-8iDKL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51pKs3SSunL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/41qzvUbEsvL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61C2A3Vx4tL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/81Rt1lFiilL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61yP+BxTTAL._AC_UL1403_.jpg",
        "https://m.media-amazon.com/images/I/71FpQZ-sb1L._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71mjTxGt2OL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/719IQhnZK8L._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "11.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B09LS63VHR",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B087Z4LJC2",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CFWPRQG",
      images: [],
      price: null,
      criteria: {
        Size: "9.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09692VR8D",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CFRDFC2",
      images: [],
      price: null,
      criteria: {
        Size: "12 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09LS7LDSZ",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B09691L1QC",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B087YPR44F",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Black/White",
      },
    },
    {
      productId: "B09693CSGJ",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B087NQG85M",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08CG1LF2S",
      images: [],
      price: null,
      criteria: {
        Size: "7.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B096919P62",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B087TMB1Z5",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B087NRN6Q9",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B087TPWKFJ",
      images: [],
      price: null,
      criteria: {
        Size: "12",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MVCD7SB",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08MV8VZ1N",
      images: [],
      price: null,
      criteria: {
        Size: "8.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B096933ZSK",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08MVBB1HK",
      images: [],
      price: null,
      criteria: {
        Size: "11 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B09692B9CD",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08CF5N54J",
      images: [],
      price: null,
      criteria: {
        Size: "13 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B087T8Q2C4",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B09692LNN5",
      images: [
        "https://m.media-amazon.com/images/I/71Sn8K+iykL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61FZZmA2EdL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/51TozMoN0qL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71QQy7rduBL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/712cJnfXbrL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71BZ8IszFQL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71G0iG-qUJL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71MXsXcywbL._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "11",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09LS7LJPP",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MVBPCBB",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B087NQDJQV",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B087Z1HMBP",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Black/White",
      },
    },
    {
      productId: "B09692FHTX",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B096943KZC",
      images: [
        "https://m.media-amazon.com/images/I/71ZVrYwGvlL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71huTAVo8RL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61uS6Bt+PEL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61hHcbPnAzL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/717Bl7CfJML._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71li5CA-LjL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71ngB+wVDlL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71EXZ1SC7XL._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B087NQ1LN7",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08MVDS1MQ",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B087TGZQRZ",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CFTM283",
      images: [],
      price: null,
      criteria: {
        Size: "10.5 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09691LK3Y",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B08MV9YNVJ",
      images: [],
      price: null,
      criteria: {
        Size: "13 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B087NQ7C9X",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B087TQBBXD",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B087Z4GTW3",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B087TGDD7T",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV99YP1",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08MVBKZKP",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08MVD2LS8",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B09691X8DH",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08MVBQNSC",
      images: [],
      price: null,
      criteria: {
        Size: "10 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B09691M6QF",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B087YPHX75",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Black/White",
      },
    },
    {
      productId: "B087TGR5B7",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09691SLJW",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09691RSKQ",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CF4T372",
      images: [],
      price: null,
      criteria: {
        Size: "11.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MVBQPX6",
      images: [],
      price: null,
      criteria: {
        Size: "12.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CF4TZ3C",
      images: [],
      price: null,
      criteria: {
        Size: "12.5 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV64R3P",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B08CF4H95M",
      images: [],
      price: null,
      criteria: {
        Size: "9 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MVB8TYY",
      images: [],
      price: null,
      criteria: {
        Size: "10.5",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B087Z1Y7S7",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "Black/White",
      },
    },
    {
      productId: "B08MV967FN",
      images: [],
      price: null,
      criteria: {
        Size: "11.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CFRXHQD",
      images: [],
      price: null,
      criteria: {
        Size: "7.5 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B08CFQCMG8",
      images: [],
      price: null,
      criteria: {
        Size: "7.5 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MV9W85P",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B09LS63661",
      images: [],
      price: null,
      criteria: {
        Size: "15",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MV9RG2T",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B08MVCJL7T",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B09691XZXT",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "White (108)/Black",
      },
    },
    {
      productId: "B09694HVJX",
      images: [
        "https://m.media-amazon.com/images/I/81xkWlwDSrL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/718k7fexwpL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/61M1nAFHUJL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71IUzDQpX7L._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/81tdJViG8VL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71SI+oxFc6L._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/81dXcrum4sL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/81F6rNmANaL._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "10",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08MV98271",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08CFPVBTW",
      images: [],
      price: null,
      criteria: {
        Size: "8 X-Wide",
        Color: "Black/White",
      },
    },
    {
      productId: "B09LS6B69G",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MV96VCY",
      images: [],
      price: null,
      criteria: {
        Size: "9.5 X-Wide",
        Color: "Mod Gray (101)/Black",
      },
    },
    {
      productId: "B08CFT4X1X",
      images: [],
      price: null,
      criteria: {
        Size: "13 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09692F6LV",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Pitch Gray (109)/Black",
      },
    },
    {
      productId: "B087T6PC5H",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08J2D6Q1Z",
      images: [],
      price: null,
      criteria: {
        Size: "14",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B08CFSGDNM",
      images: [],
      price: null,
      criteria: {
        Size: "15 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV9W14W",
      images: [],
      price: null,
      criteria: {
        Size: "10",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B096931RWC",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B09693QY4L",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Victory Blue (403)/Midnight Navy Blue",
      },
    },
    {
      productId: "B08CF4NZ7N",
      images: [],
      price: null,
      criteria: {
        Size: "11 X-Wide",
        Color: "White(100)/Black",
      },
    },
    {
      productId: "B08MVBKRJP",
      images: [],
      price: null,
      criteria: {
        Size: "8",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B08MV9NCTP",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Black (004)/Royal",
      },
    },
    {
      productId: "B087T8J7DW",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09692P27K",
      images: [],
      price: null,
      criteria: {
        Size: "8.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08MVF8BCM",
      images: [
        "https://m.media-amazon.com/images/I/51Ms5hzZGPL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/514Eod8cJhL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/61oNAahqqeL._AC_UL1499_.jpg",
        "https://m.media-amazon.com/images/I/61BRxCM5TUL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/51gEX7TiD0L._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/6125F8A0oeL._AC_UL1000_.jpg",
        "https://m.media-amazon.com/images/I/814JwxJ0TjL._AC_UL1500_.jpg",
        "https://m.media-amazon.com/images/I/71e6iOwpvKL._AC_UL1500_.jpg",
      ],
      price: null,
      criteria: {
        Size: "11",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B087NQ93ZK",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Mod Gray (101)/White",
      },
    },
    {
      productId: "B087TFFP3W",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B087T5QNBY",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09LS8HCWT",
      images: [],
      price: null,
      criteria: {
        Size: "9",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08MV8FBXR",
      images: [],
      price: null,
      criteria: {
        Size: "13",
        Color: "Red (600)/White",
      },
    },
    {
      productId: "B09LS7KX2D",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B087T3L241",
      images: [],
      price: null,
      criteria: {
        Size: "7",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CFNHCG8",
      images: [],
      price: null,
      criteria: {
        Size: "8 X-Wide",
        Color: "Academy Blue (400)/White",
      },
    },
    {
      productId: "B09LS7DNTH",
      images: [],
      price: null,
      criteria: {
        Size: "11",
        Color: "(119) Pitch Gray/Capri/Black",
      },
    },
    {
      productId: "B08CG1K9QH",
      images: [],
      price: null,
      criteria: {
        Size: "14 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08CG1K9QK",
      images: [],
      price: null,
      criteria: {
        Size: "9 X-Wide",
        Color: "Black (002)/Black",
      },
    },
    {
      productId: "B08MV9LG2J",
      images: [],
      price: null,
      criteria: {
        Size: "9.5",
        Color: "Black (005)/Blaze Orange",
      },
    },
    {
      productId: "B08MVBG39Z",
      images: [],
      price: null,
      criteria: {
        Size: "12.5",
        Color: "Blue Note (402)/White",
      },
    },
    {
      productId: "B09692FQRC",
      images: [],
      price: null,
      criteria: {
        Size: "11.5",
        Color: "Black (007)/White",
      },
    },
    {
      productId: "B08MV9HCWK",
      images: [],
      price: null,
      criteria: {
        Size: "7.5",
        Color: "Black (004)/Royal",
      },
    },
  ],
  weight: null,
  specifications: [
    {
      name: "Item model number",
      value: "3024857",
    },
    {
      name: "Department",
      value: "Mens",
    },
    {
      name: "Date First Available",
      value: "April 25, 2020",
    },
    {
      name: "Manufacturer",
      value: "Under Armour",
    },
    {
      name: "ASIN",
      value: "B087P32XQG",
    },
    {
      name: "Best Sellers Rank",
      value: "See Top 100 in Clothing, Shoes & Jewelry",
    },
  ],
  images: [
    "https://m.media-amazon.com/images/I/410-L0vF3+L._AC_UL1000_.jpg",
    "https://m.media-amazon.com/images/I/51hlA-8iDKL._AC_UL1000_.jpg",
    "https://m.media-amazon.com/images/I/51pKs3SSunL._AC_UL1000_.jpg",
    "https://m.media-amazon.com/images/I/41qzvUbEsvL._AC_UL1000_.jpg",
    "https://m.media-amazon.com/images/I/61C2A3Vx4tL._AC_UL1000_.jpg",
    "https://m.media-amazon.com/images/I/81Rt1lFiilL._AC_UL1500_.jpg",
    "https://m.media-amazon.com/images/I/61yP+BxTTAL._AC_UL1403_.jpg",
    "https://m.media-amazon.com/images/I/71FpQZ-sb1L._AC_UL1500_.jpg",
    "https://m.media-amazon.com/images/I/71mjTxGt2OL._AC_UL1500_.jpg",
    "https://m.media-amazon.com/images/I/719IQhnZK8L._AC_UL1500_.jpg",
  ],
  image: "https://m.media-amazon.com/images/I/410-L0vF3+L.jpg",
};
const createIdMapper = (productDetails) => {
  /**
   * This gives unique selection of variant options to display to the user
    {
    Capacity: Set(2) { '256GB', '512GB' },
    Color: Set(3) { 'Silver', 'Space Gray', 'Gold' }
    }

    complexity of O(n^2)
   */
  const idMapper = {};
  const variantIdMapper = {};
  for (const variant of productDetails.variants) {
    variantIdMapper[variant.productId] = variant;
    for (const [criteria, criteriaValue] of Object.entries(variant.criteria)) {
      idMapper[criteria] = new Set(idMapper[criteria]);
      idMapper[criteria].add(criteriaValue);
    }
  }

  return [idMapper, variantIdMapper];
};

const [variantGroup] = createIdMapper(product);
const variantKeys = Object.keys(variantGroup);
const [idMapper, variantIdMapper] = createIdMapper(product);

const getVariantId = (selection, productVariants) => {
  /**
   * This returns the exact variant based on the user selection or null if variant was not found
   *
   *  complexity of O(n^2)
   */
  let variantId = null;
  for (const variant of productVariants) {
    let foundSet = new Set();
    for (const [key, value] of Object.entries(variant.criteria)) {
      if (selection[key] == value) {
        foundSet.add(true);
      } else {
        foundSet.add(false);
      }
    }
    if (foundSet.has(false)) {
      continue;
    } else {
      variantId = variant.productId;
      break;
    }
  }
  if (!variantId) return null;
  return variantIdMapper[variantId];
};

const selection = {
  Size: "7.5",
  Color: "Black (004)/Royal",
};
const selected = getVariantId(selection, product.variants);

const getCurrentProduct = product.variants.find(
  (product) => product.productId === "B087YYGSBY"
);
