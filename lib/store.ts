import create from 'zustand';
import { devtools } from 'zustand/middleware';

const storeData = (set) => ({
  cartItems: [],
  updateCart: (cartItems) => {
    set({ cartItems });
  },
  searchResults: {
    data: {
      data: [],
      pagination: {
        current: 1,
        perPage: 10,
        totalPages: 0,
        totalResults: 0,
      },
    }
  },
  updateResult: (result) => {
    set({ searchResults: result })
  },
  isLoading: true,
  updateLoading: (isLoading) => {
    set({ isLoading })
  }
});

export const useStoreData = create(devtools(storeData));
