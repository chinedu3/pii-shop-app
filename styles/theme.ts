import { extendTheme } from "@chakra-ui/react"
import { ButtonStyles as Button } from './components/buttonStyles'
const theme = extendTheme({
  colors: {
    brand: {
      100: "#f7fafc",
      // ...
      900: "#1a202c",
    },
    lime:
    {
      "50": "#000",
      "100": "#000",
      "200": "#000",
      "300": "#000",
      "400": "#000",
      "500": "#000",
      "600": "#000",
      "700": "#000",
      "800": "#000",
      "900": "#000"
    }
  },
  fonts: {
    heading: "Inter",
    body: "Inter",
  },
  components: {
    Button, // Has to match to the name of the component

  },
})

export default theme;