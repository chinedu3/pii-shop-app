import { darken, mode } from "@chakra-ui/theme-tools";

export const ButtonStyles = {
  // style object for base or default style
  baseStyle: {
    outline: "none",
    _focus: { boxShadow: "none" },
  },
  // styles for different sizes ("sm", "md", "lg")
  sizes: {},
  // styles for different visual variants ("outline", "solid")
  variants: {
    primary: (props) => ({
      bg: "rgba(196, 196, 196, 0.25)",
      borderRadius: "30px",
      fontSize: "16px",
      fontWeight: "400",
      _hover: {
        bg: mode("#3E3E3E", darken("#FCDD18", 20))(props),
        boxShadow: "md",
        color: "#fff",
        _disabled: {
          bg: "#c4c4c440",
          color: "black",
        },
      },
    }),
    secondary: () => ({
      bg: "#FFF",
      borderRadius: "30px",
      fontSize: "14px",
      fontWeight: "400",
      boxShadow: "0px 1px 3px rgba(0, 0, 0, 0.2), 0px 0px 8px rgba(0, 0, 0, 0.06)",
      _hover: {
        bg: "#E5EBF5",
        boxShadow: "md",
        outline: "none",
      },
    }),
    secondaryOutline: () => ({
      bg: "#3E3E3E",
      border: "1px solid",
      borderRadius: "4px",
      transition: "all 200ms ease",
      color: "#ffffff",
      _hover: {
        bg: "transparent",
        color: "#000000",

        boxShadow: "md",
        transform: "scale(1.02)",
      },
      _focus: {
        outline: "none",
      },
    }),
  },
  // default values for `size` and `variant`
  defaultProps: {
    variant: "primary",
  },
};
