import { useMediaQuery } from '@chakra-ui/react';

/**
 * Tells us if the user is on a mobile device or not
 * Our definition of mobile is devices with max-width: 600px
 * @returns Boolean
 */
export function useIsMobile() {
  const [isMobile] = useMediaQuery('(max-width: 600px)');
  return isMobile;
}

export const formatCurrency = (value, currency) => {
  return new Intl.NumberFormat(("en-NG"), {
    style: "currency",
    minimumFractionDigits: 0,
    currency,
    currencyDisplay: 'narrowSymbol'

  }).format(value);
};

export const createIdMapper = (productDetails) => {
  /**
   * This gives unique selection of variant options to display to the user
    {
    Capacity: Set(2) { '256GB', '512GB' },
    Color: Set(3) { 'Silver', 'Space Gray', 'Gold' }
    }

    complexity of O(n^2)
   */
  const idMapper = {};
  const variantIdMapper = {};
  for (const variant of productDetails.variants) {
    variantIdMapper[variant.productId] = variant;
    for (const [criteria, criteriaValue] of Object.entries(variant.criteria)) {
      idMapper[criteria] = new Set(idMapper[criteria]);
      idMapper[criteria].add(criteriaValue);
    }
  }

  return [idMapper, variantIdMapper];
};

// export const getVariantId = (selection, productVariants) => {
//   /**
//    * This returns the exact variant based on the user selection or null if variant was not found
//    *
//    *  complexity of O(n^2)
//    */
//   let variantId = null;
//   for (const variant of productVariants) {
//     let foundSet = new Set();
//     for (const [key, value] of Object.entries(variant.criteria)) {
//       if (selection[key] == value) {
//         foundSet.add(true);
//       } else {
//         foundSet.add(false);
//       }
//     }
//     if (foundSet.has(false)) {
//       continue;
//     } else {
//       variantId = variant.productId;
//       break;
//     }
//   }
//   if (!variantId) return null;
//   return variantId;
// };


// --------------


export const getVariantGroup = (product: any) => {
  const allCriteriasWithIds: any = [];
  const variantGroup = {};
  for (const variant of product.variants) {
    const { criteria, productId } = variant;
    allCriteriasWithIds.push({ criteria, productId: productId });

    for (const [name, value] of Object.entries(criteria)) {
      if (!variantGroup[name]) {
        variantGroup[name] = new Set();
      }
      variantGroup[name].add(value);
    }
  }

  for (const [groupKey, uniqueValues] of Object.entries(variantGroup)) {
    variantGroup[groupKey] = [...uniqueValues as any].sort((a, b) => {
      const aIsNumber = Number.isFinite(+a);
      const bIsNumber = Number.isFinite(+b);

      if (aIsNumber && bIsNumber) {
        return a - b;
      }

      if (aIsNumber && !bIsNumber) return -1;
      if (!aIsNumber && bIsNumber) return 1;

      if (a.toLowerCase() > b.toLowerCase()) return 1;
      if (a.toLowerCase() < b.toLowerCase()) return -1;
      return 0;
    });
  }

  return { allCriteriasWithIds, variantGroup };
};



const isSubset = (superObj, subObj) => {
  // checks if an object is subset of another
  return Object.keys(subObj).every((ele) => {
    if (typeof subObj[ele] == "object") {
      return isSubset(superObj[ele], subObj[ele]);
    }
    return subObj[ele] === superObj[ele];
  });
};

export const getValidOptions = (
  selection,
  criteriaKey,
  allCriteriasWithIds,
  variantGroup
) => {
  const enabledSet = new Set();
  for (const { criteria } of allCriteriasWithIds) {
    const copy = { ...criteria };
    delete copy[criteriaKey];
    if (isSubset(selection, copy)) {
      enabledSet.add(criteria[criteriaKey]);
    }
  }

  const valuesInfo: any = [];
  for (const value of variantGroup[criteriaKey]) {
    const item = { value, enabled: enabledSet.has(value) };
    valuesInfo.push(item);
  }

  return valuesInfo;
};

export const getVariantId = (selection, criteriasWithId, criteriaKeyWhenNotFound) => {
  let variantId = null;

  let defaultIdWhenNotFound = null;
  for (const { criteria, productId } of criteriasWithId) {
    let foundSet = new Set();

    if (
      !defaultIdWhenNotFound &&
      selection[criteriaKeyWhenNotFound] === criteria[criteriaKeyWhenNotFound]
    ) {
      defaultIdWhenNotFound = productId;
    }
    for (const [key, value] of Object.entries(criteria)) {
      if (selection[key] == value) {
        foundSet.add(true);
      } else {
        foundSet.add(false);
      }
    }
    if (foundSet.has(false)) {
      continue;
    } else {
      variantId = productId;
      break;
    }
  }
  return variantId || defaultIdWhenNotFound;
};


