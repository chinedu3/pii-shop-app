export const BASE_URL = process.env.NEXT_PUBLIC_API_BASE_URL;

export const MERCHANT_LOGO = {
  jumia: "https://res.cloudinary.com/pii/image/upload/v1647978932/jumia_uucsjk.png",
  konga: "https://res.cloudinary.com/pii/image/upload/v1647978932/konga_mhltdx.png",
  walmart: "https://res.cloudinary.com/pii/image/upload/v1647978932/walmart_b3fhdy.png",
  amazon: "https://res.cloudinary.com/pii/image/upload/v1647978932/amazon_uhqjyz.png",
  ebay: "https://res.cloudinary.com/pii/image/upload/v1647978932/ebay_f011bi.png"

}

export const PII_LOGO = "https://res.cloudinary.com/pii/image/upload/v1648455213/piiLogo_ditjsv.png";

export const NAV_ICONS = {
  ALERT: "https://res.cloudinary.com/pii/image/upload/v1648465777/Alert_njnpcb.png",
  CART: "https://res.cloudinary.com/pii/image/upload/v1648465777/Cart_isykum.png",
  HOME: "https://res.cloudinary.com/pii/image/upload/v1648465777/Home_rwlfrf.png",
}

export type BasicProductType = {
  image: string
  merchant: string
  oldPrice: { currency: string, value: number }
  price: { currency: string, value: number }
  productId: string
  title: string
}

type VariantType = {
  productId: string,
  images: Array<string>,
  price: any,
  criteria: any
}
export interface ProductType extends BasicProductType {
  variants: Array<VariantType>
  doVariantsHaveProductPage: boolean
  images: Array<string>
  rating: number
  in_stock: boolean
}

export const BasicProductDefault = {
  image: "",
  merchant: "",
  oldPrice: 0,
  price: { currency: "NGN", value: 0 },
  productId: "",
  title: "",
}