
export const addItemToCart = (cartItems: any, cartItemToAdd: any) => {
  const existingCartItem = cartItems.find(
    (cartItem: any) => cartItem.productId === cartItemToAdd.productId
  );

  if (existingCartItem) {
    return cartItems.map((cartItem: any) =>
      cartItem.productId === cartItemToAdd.productId
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }

  return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const removeItemFromCart = (cartItems: any, cartItemToRemove: any) => {
  const existingCartItem = cartItems.find(
    (cartItem: any) => cartItem.productId === cartItemToRemove.productId
  );

  if (existingCartItem.quantity === 1) {
    return cartItems.filter(
      (cartItem: any) => cartItem.productId !== cartItemToRemove.productId
    );
  }

  return cartItems.map((cartItem: any) =>
    cartItem.productId === cartItemToRemove.productId
      ? { ...cartItem, quantity: cartItem.quantity - 1 }
      : cartItem
  );
};

export const deleteItemFromCart = (cartItems: any, cartItemToRemove: any) =>
  cartItems.filter((cartItem: any) => cartItem._id !== cartItemToRemove._id);

export const getCartItemCount = (cartItems: any) => {
  if (!cartItems) {
    return 0;
  }
  return cartItems.reduce(
    (accumalatedQuantity: number, cartItem: any) =>
      accumalatedQuantity + cartItem.quantity,
    0
  );
};

// calculates the total
export const getCartTotal = (cartItems: any) => {
  if (!cartItems) {
    return 0;
  }
  return cartItems.reduce(
    (accumalatedQuantity: number, cartItem: any) =>
      accumalatedQuantity + (cartItem.price.value * cartItem.quantity),
    0
  );
};

export const addItemToCartByNumber = (
  cartItems: any,
  cartItemToAdd: any,
  quantity: number,
  bidPrice: number,
  isPcs: boolean
) => {
  if (!cartItems) {
    return [];
  }
  const existingCartItem = cartItems.find(
    (cartItem: any) => cartItem._id === cartItemToAdd._id
  );
  if (existingCartItem) {
    return cartItems.map((cartItem: any) =>
      cartItem._id === cartItemToAdd._id
        ? { ...cartItem, quantity, bidPrice, isPcs }
        : cartItem
    );
  }
  return [
    ...cartItems,
    { ...cartItemToAdd, quantity, bidPrice, isPcs },
  ];
};

export const getQuantityFromCart = (cartItems: any, product: any) => {
  if (!cartItems) {
    return 0;
  }
  const item = cartItems.find((cartItem: any) => cartItem._id === product._id);
  if (item?.quantity) {
    return item.quantity;
  }
  return 0;
};
