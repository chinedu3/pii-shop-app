import React, { useCallback, useRef, useState } from "react";
import axiosInstance, { productRequest } from "services";
import { BASE_URL } from "utlis/constant";
import { convertParamsToString } from "services/helper";
import { Box, useBoolean } from "@chakra-ui/react";
import axios from "axios";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import toast from "react-hot-toast";
import MobilePage from "components/productInfo/MobilePage";
import DesktopPage from "components/productInfo/Desktop";
export const defaultPagination = {
  current: 1,
  perPage: 10,
  totalPages: 0,
  totalResults: 0,
};
const ProductDetails = (props) => {
  const [showReview, setShowReview] = useBoolean();
  const history = useRouter();
  const query: string = String(history.query.productId);
  const [productId, merchantName] = query.split("-");

  const [productReviews, setProductReviews] = useState({
    data: [],
    pagination: defaultPagination,
  });
  const [pageNo, setPageNo] = useState(1);
  const getReviews = async (params: any) => {
    const { queryKey } = params;
    const [, pageNo] = queryKey;

    const url = convertParamsToString(productRequest.PRODUCT_REVIEW, {
      productId,
      merchantName,
      pageNo,
    });
    const response = await axios.get(BASE_URL + url);
    return response.data;
  };
  const [shouldAppend, setShouldAppend] = useBoolean();
  const [hasMore, sethasMore] = useState(false);

  const addReviewsToState = (response) => {
    const newReviews = response.data;
    const pagination = response.pagination || defaultPagination;
    sethasMore(
      productReviews.data.length !== productReviews.pagination.totalResults
    );
    if (shouldAppend) {
      const updatedReviews: any = [...productReviews.data, ...newReviews];
      setProductReviews({ data: updatedReviews, pagination });
      return;
    }
    setProductReviews({
      data: newReviews,
      pagination,
    });
  };

  const { isFetching } = useQuery(
    ["QUERY_KEY.GETREPO_KEY", pageNo],
    getReviews,
    {
      keepPreviousData: true,
      onSuccess: (res) => addReviewsToState(res),
      onError: () => toast.error("An error occured while fetching reviews"),
    }
  );
  const loadMore = useCallback(() => {
    if (pageNo >= productReviews.pagination.totalPages) return;
    setPageNo((prevPageNumber) => prevPageNumber + 1);
    setShouldAppend.on();
  }, [pageNo, productReviews.pagination.totalPages, setShouldAppend]);

  const observer = useRef<any>();
  const lastBookElementRef = useCallback(
    (node) => {
      if (isFetching) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          loadMore();
        }
      });
      if (node) observer.current.observe(node);
    },
    [isFetching, hasMore, loadMore]
  );

  const product = props.productInfo;
  return (
    <>
      {
        !props.success ? <Box>Product Not Found</Box> : <>
          <Box display={{ base: "none", md: "block" }}>
            <DesktopPage
              product={product}
              productReviews={productReviews}
              loadMore={loadMore}
              isFetching={isFetching}
            />
          </Box>
          <Box display={{ base: "block", md: "none" }}>
            <MobilePage
              product={product}
              showReview={showReview}
              setShowReview={setShowReview}
              productReviews={productReviews}
              lastBookElementRef={lastBookElementRef}
              isFetching={isFetching}
            />
          </Box>
        </>
      }
    </>
  );
};

export async function getServerSideProps(context) {
  try {
    const [productId, merchantName] = context.params.productId.split("-");
    const tempUrl = convertParamsToString(productRequest.PRODUCT_INFO, {
      productId,
      merchantName,
    });
    const finalURl = `${BASE_URL}${tempUrl}`;
    const productRes = await axiosInstance.get(finalURl);
    return {
      props: {
        success: true,
        productInfo: productRes.data.data,
      },
    };
  } catch (error: any) {
    return {
      props: {
        success: false,
      },
    };
  }
}

export default ProductDetails;
