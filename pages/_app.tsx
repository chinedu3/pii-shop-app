import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "styles/theme";
import { Toaster } from 'react-hot-toast';
import { ReactQueryDevtools } from 'react-query/devtools';
import { QueryClient, QueryClientProvider } from 'react-query';
import React from 'react';

const RenderDevTool = () => {
  if (process.env.NODE_ENV === 'development') {
    return <ReactQueryDevtools initialIsOpen={false} />;
  }
  return null;
};

const MyApp = (props: AppProps) => {
  const queryClient = new QueryClient();
  const { Component, pageProps } = props;
  return <QueryClientProvider client={queryClient}>
    <ChakraProvider theme={theme}>
      <Toaster />
      <Component {...pageProps} />
    </ChakraProvider>
    <RenderDevTool />
  </QueryClientProvider>

}

export default MyApp;
