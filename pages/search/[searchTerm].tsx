import React from "react";
import SearchDesktop from "components/search/SearchDesktop";
import { useQuery } from "react-query";
import axios from "axios";
import toast from "react-hot-toast";
import { productRequest } from "services";
import { convertParamsToString } from "services/helper";
import { BASE_URL } from "utlis/constant";
import { useRouter } from "next/router";
import Loader from "components/Loader";
import SearchMobile from "components/search/searchMobile";
import { useIsMobile } from "utlis/hooks";
import { defaultPagination } from "pages/product/[productId]";
import { SEARCH_KEY } from 'services/queryKeys';
import { useStoreData } from 'lib/store';

const SearchPage = () => {
  const isMoblie = useIsMobile();
  const history = useRouter();
  const [searchResult, udpateResult, isLoading, updateLoading] = useStoreData(state => [state.searchResults,
  state.updateResult,
  state.isLoading,
  state.updateLoading
  ])
  const getProducts = async () => {
    const url = convertParamsToString(productRequest.SEARCH, {
      search: history.query.searchTerm,
      page: "1",
    });
    const response = await axios.get(BASE_URL + url);
    return response.data;
  };

  const updateState = (res) => {
    udpateResult({
      data: res
    })
    updateLoading(false);
  }
  useQuery(
    [SEARCH_KEY], getProducts,
    {
      enabled: !!history.query.searchTerm,
      onSuccess: (res) => updateState(res),
      onError: () => toast.error("An error occured while fetching reviews"),
    }
  );
  // 1440px
  const defaultState = {
    data: [],
    pagination: defaultPagination,
  };
  return (
    <>
      {isLoading ? (
        <Loader isFullHeight />
      ) : isMoblie ? (
        <SearchMobile data={searchResult.data || defaultState} />
      ) : (
        <SearchDesktop products={searchResult.data || defaultState} />
      )}
    </>
  );
};

export default SearchPage;
