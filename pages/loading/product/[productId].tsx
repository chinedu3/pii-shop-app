import React from 'react'
import { useRouter } from 'next/router';
import NavBar from 'components/NavBar';
import { Box } from '@chakra-ui/react';
import Loader from 'components/Loader';
const Product = () => {
  const history = useRouter();
  React.useEffect(() => {
    history.replace(`/product/${history.query.productId}`);
  })
  return (
    <>
      <NavBar />
      <Box mt="5rem">
        <Loader loadingText='Fecting Product...' />
      </Box>
    </>
  )
}

export default Product