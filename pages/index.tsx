import type { NextPage } from 'next'
import { Flex, Box } from '@chakra-ui/layout'
import { Input, InputGroup, InputRightElement, Text, Heading } from '@chakra-ui/react'
import { AiOutlineSearch } from 'react-icons/ai';
import { BsArrowUpRight } from 'react-icons/bs';

import PiLogo from '../assets/Pii Logo.png'
import Image from 'next/image'
import { nanoid } from 'nanoid';
import { useState } from 'react';
import { useRouter } from 'next/router';
const Home: NextPage = () => {
  const [searchTerm, setSearchTerm] = useState("")
  const history = useRouter();
  const handleSearch = () => {
    history.push(`/search/${searchTerm}`)
  }
  return (
    <Flex bg="#171717" h="100vh"
      justifyContent="center"
      alignItems="center"
      flexDir="column"
      position="relative"
    >
      <Box position="absolute"
        top="42px"
        left="33px"
      >
        <Image src={PiLogo} alt="pi Logo" />
      </Box>
      <Box px="4">
        <Heading as="h1"
          color="#fff"
          fontWeight="normal"
          fontFamily="Rozha One"
          fontSize={{ base: "38px", md: "48px" }}
          mt={{ base: "-16", md: "0" }}
          lineHeight="38px"
        >
          THE BEST.<Box as="span"
            color="#979696"
            display={{ base: "inline-block" }}
          > SHOPPING EXPERIENCE.</Box> EVER.
        </Heading>
        <Box w="100%">
          <InputGroup
            transition="all 500ms ease-in-out"
            background="rgba(0, 0, 0, 0.63)"
            maxWidth="600px"
            margin=" 1rem auto"
            borderRadius="30px"
            border="1px solid #565656"
            height=" 61px"
            alignItems="center"
          >
            <Input
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  handleSearch()
                }
              }}
              onChange={(e) => setSearchTerm(e.target.value)}
              placeholder='Search'
              _placeholder={{
                color: "#979696"
              }}
              color="#fff"
              _focus={{
                outline: 'none'
              }}
              fontSize="16px"
              width=" 80%"
              border="none"
              margin=" auto 0"
              marginLeft=" 1rem"
              height=" 45px"
              outline=" none"

            />
            <InputRightElement h="100%" mr="3"
              onClick={() => handleSearch()}
            >
              <AiOutlineSearch color="#979696" size="23px" /></InputRightElement>
          </InputGroup>
          <Box maxW={"550px"}
            margin="0 auto"
          >


            {
              // add item to the array to show search options
              new Array(0).fill(0).map(() =>
                <Flex key={nanoid()} mb="4" alignItems={"center"} justifyContent="space-between" color="#fff">
                  <Flex alignItems={"center"} >
                    <AiOutlineSearch color="#979696" size="23px" />
                    <Text
                      ml="10px"
                      fontSize={"14px"}
                      lineHeight={"4"}
                      color="#C4C4C4"
                    >Baby Puma</Text>
                  </Flex>
                  <BsArrowUpRight />
                </Flex>
              )
            }
          </Box>
        </Box>
      </Box>
    </Flex >
  )
}

export default Home