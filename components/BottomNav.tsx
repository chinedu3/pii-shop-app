/* eslint-disable jsx-a11y/alt-text */
import { Avatar, Image, Box, Text, Flex } from '@chakra-ui/react'
import { useStoreData } from 'lib/store'
import React from 'react'
import { AiFillHeart } from 'react-icons/ai'
import { NAV_ICONS } from 'utlis/constant'

const BottomNav = () => {
  const [cartItems] = useStoreData((state) => [
    state.cartItems,
  ]);
  return (
    <Flex
      justifyContent="space-between"
      pt="0.5rem"
      px="4"
      background="#FFFFFF"
      boxShadow="0px 0px 12px rgba(0, 0, 0, 0.15)"
      position="absolute"
      w="100%"
      bottom="0"
      zIndex="29"
    >
      <Flex
        alignItems="center"
        flexDir="column"
      >
        <Image src={NAV_ICONS.HOME} />
        <Text
          fontSize="12px"
          color="#C4C4C4"
        >Home</Text>
      </Flex>
      <Flex
        alignItems="center"
        flexDir="column"
      >
        <Image src={NAV_ICONS.ALERT} />
        <Text
          fontSize="12px"
          color="#C4C4C4"
        >Alert</Text>
      </Flex>
      <Flex
        alignItems="center"
        flexDir="column"
        position="relative"
      >
        <Image src={NAV_ICONS.CART} />
        <Text
          fontSize="12px"
          color="#C4C4C4"
        >Cart</Text>
        {cartItems.length ? <Flex position="absolute"
          bg="#DE0000"
          color="#fff"
          fontSize="10px"
          borderRadius="20px"
          top="-5px"
          right="-10px"
          w="24px"
          justifyContent="center"
        >
          {cartItems.length}
        </Flex> : null}
      </Flex>
      <Flex alignItems="center"
        alignContent="center"
        flexDir="column"
      >
        <Avatar src="https://picsum.photos/200/300"
          width="25px"
          height="25px"
          border="2px solid #fff"
        />
        <Text
          fontSize="12px"
          color="#C4C4C4"
        >Profile</Text>
      </Flex>
    </Flex>
  )
}

export default BottomNav