/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from "react";
import { Flex, Image, Box, Input, useBoolean, Button } from "@chakra-ui/react";
import { PII_LOGO } from "utlis/constant";
import { AiOutlineSearch } from 'react-icons/ai'
import { MdArrowBackIos, MdOutlineKeyboardBackspace } from 'react-icons/md'
import { useRouter } from 'next/router'
import { PUBLIC_PATH } from 'routes';
import { convertParamsToString } from 'services/helper';

const NavBar = () => {

  const [search, setSearch] = useState<any>("");
  const [showSearchDiaglog, setSearchDialog] = useBoolean();
  const history = useRouter();
  const handleSearch = () => {
    if (search) {
      const page = convertParamsToString(PUBLIC_PATH.SEARCH, { searchTerm: search })
      setSearchDialog.off()
      history.push(page)
    } else {
      setSearchDialog.off()
    }
  }
  const [isSearchPage, setisSearchPage] = useState(false)
  useEffect(() => {
    const searchTerm = history.query.searchTerm || ""
    setSearch(searchTerm)
    if (history.pathname.includes("search")) {
      setisSearchPage(true)
    } else {
      setisSearchPage(false)
    }
  }, [history.pathname, history.query.searchTerm])

  return (
    <Box
      position="sticky"
      top="0"
      zIndex="30"
    >
      {isSearchPage ?
        <Flex
          bg="#fff"
          p="0.5rem"
          borderBottom="3px solid #E7E8E9"
          alignItems="center"
        >
          <Button
            variant="unstyled"
            m="0"
            onClick={() => history.back()}
          >
            <MdOutlineKeyboardBackspace size={25} color="#C4C4C4" />
          </Button>
          <Input h="30px" bg="#F0F0F0" border="#F0F0F0"
            fontSize="14px"
            type="search"
            ml="-1"
            readOnly
            value={search}
            onClick={setSearchDialog.on}
            _focus={{
              outline: "none"
            }}
          />
        </Flex>
        :
        <Flex bg="#000" color="#fff"
          justifyContent="space-between"
          p="3"
        >
          <Image src={PII_LOGO} alt="pii sub image"
            h="24px"
          />
          <Flex
            _hover={{
              cursor: "pointer"
            }}
            onClick={setSearchDialog.toggle}
          ><AiOutlineSearch size={25} />
          </Flex>
        </Flex>}
      {
        showSearchDiaglog &&

        <Box
          position="absolute"
          w="100%"
          bg="#4B4A4A"
          h="100vh"
          inset="0"
          zIndex="30"
          p="0.5rem 1rem"
          overflow="hidden"
        >
          <Flex
            alignItems="center"
            borderBottom="1px solid #979696"
            position="sticky"
            top="0"
          >
            <Box
              onClick={setSearchDialog.toggle}
            >
              <MdArrowBackIos size={25} color="#fff" />
            </Box>
            <Input
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              ml="-2"
              placeholder='Search product, brands, etc'
              _placeholder={{
                color: "#979696"
              }}
              color="#fff"
              outline="none"
              border="none"
              _focus={{
                boxShadow: "none"
              }}
            />
            <Box
              onClick={handleSearch}
            ><AiOutlineSearch size={25} color="#fff" />
            </Box>
            {search.length ? <Box ml="3"
              onClick={() => setSearch("")}
            >
              <Image src="https://res.cloudinary.com/pii/image/upload/v1648459421/closeBtn_uuanas.png"
                h="25px"
                w="25px"
                objectFit="contain"
              />
            </Box> : null}
          </Flex>
        </Box>
      }
    </Box>
  );
};

export default NavBar;
