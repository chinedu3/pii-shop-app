import { useBoolean, Box, Flex, Image, Text, Button, useClipboard } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { MERCHANT_LOGO } from "utlis/constant";
import { formatCurrency } from "utlis/hooks";
import ShareIcon from "components/ShareIcon";
import LikeIcon from "components/LikeIcon";
import FbIcon from "assets/fb.svg";
import TwitterIcon from "assets/twitter.svg";
import CopyIcon from "assets/copy.svg";
import NextImage from "next/image";
import {
  FacebookShareButton,
  TwitterShareButton
} from "react-share";
import { useRouter } from 'next/router'
import { useSelectedProduct } from './SearchDesktop/store';

const ProductCardDesktop = (props) => {
  const { product, handleLike } = props;

  const [showBtn, setshowBtn] = useBoolean();
  const [isShareVisible, setShareVisiblility] = useBoolean();
  const [updateProduct] = useSelectedProduct(state => [state.updateProduct])
  const [isLiked, setIsLiked] = useState(false);
  const price = product?.price.value / 100 || 0;
  let [whole, decimal] = price.toString().split(".");
  whole = formatCurrency(whole, product.price.currency);
  let numDecimal: string | number = Number(decimal);
  numDecimal = !numDecimal
    ? "00"
    : +numDecimal < 10
      ? numDecimal * 10
      : numDecimal;

  const onLike = () => {
    setIsLiked(!isLiked);
    handleLike();
    updateProduct(product)
  };
  const [shareUrl, setshareUrl] = useState("")
  const [title, setTitle] = useState("")
  useEffect(() => {
    const host = window.location.host;
    const protocol = `${window.location.protocol}//`;
    const shareUrl = `${protocol}${host}/product/${product.productId}-${product.merchant}`
    const title = product.title;
    setshareUrl(shareUrl);
    setTitle(title)
    //eslint-disable-next-line
  }, []);
  const history = useRouter();
  const productId = product.productId || product.id;
  const merchantName = product.merchant;
  const { hasCopied, onCopy } = useClipboard(shareUrl)
  return (
    <Box
      borderRadius="4px"
      textAlign="center"
      transition="all 0.2s ease"
      _hover={{
        boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.24)",
        transform: "scale(1.05)",
        cursor: "pointer",
        borderRadius: "4",
      }}
      onMouseEnter={setshowBtn.on}
      onMouseLeave={setshowBtn.off}
      w="100%"
    >
      <Flex justifyContent="center" p="4" position="relative" m="auto">
        <Image src={product.image} alt="product"
          w="304px"
          h="250px"
          objectFit="contain"
          margin="0 auto"
        />
        <ShareIcon onClick={setShareVisiblility.toggle} />
        <LikeIcon handleLikeorDislike={onLike} isLiked={isLiked} />
      </Flex>
      <Flex p="4" position="relative">
        <Text
          mt="2"
          textAlign="center"
          w="100%"
          noOfLines={2}
          h="48px"
          textTransform="capitalize"
          onClick={() => history.push(`/loading/product/${productId}-${merchantName}`)
          }
        >
          {product.title}
        </Text>
        <Flex
          display={isShareVisible ? "flex" : "none"}
          position="absolute"
          top="0"
          right="15px"
          bg="#fff"
          flexDir="column"
          boxShadow="0px 0px 3.79544px rgba(0, 0, 0, 0.25)"
          borderRadius="7px"
          py="4"
        >
          <FacebookShareButton
            url={shareUrl}
            title={title}
          >
            <Box
              h="40px"
              fontWeight="normal"
              d="flex"
              alignItems="center"
              pl="4"
              pr="4"
              _hover={{
                bg: "#F5F5F6"
              }}
            >

              <Box as="span" display="flex" alignItems="center" mr="13px">
                <NextImage src={FbIcon} height={20} width={20} alt="this is icon" />
              </Box>
              <Text> Facebook</Text>
            </Box>
          </FacebookShareButton>

          <TwitterShareButton
            url={shareUrl}
            title={title}
          >
            <Box
              pl="4"
              pr="4"
              h="40px"
              fontWeight="normal"
              d="flex"
              alignItems="center"
              _hover={{
                bg: "#F5F5F6"
              }}
            >
              <Box as="span" display="flex" alignItems="center" mr="13px">
                <NextImage
                  src={TwitterIcon}
                  height={20}
                  width={20}
                  alt="this is icon"
                />
              </Box>
              <Text w="74px"> Twitter</Text>

            </Box>
          </TwitterShareButton>
          <Button
            _hover={{
              bg: "#F5F5F6"
            }}
            variant="unstyled"
            fontWeight="normal"
            d="flex"
            alignItems="center"
            onClick={onCopy}
          >

            <Box as="span" display="flex" alignItems="center" mr="13px">
              <NextImage src={CopyIcon} height={20} width={20} alt="this is icon" />
            </Box>

            <Text w="74px"> {hasCopied ? 'Copied' : 'Copy'}</Text>
          </Button>
        </Flex>
      </Flex>
      <Image
        src={MERCHANT_LOGO[product.merchant]}
        alt="merchanticon"
        m="0 auto"
        mb="2"
      />
      <Box textAlign="center">
        <Text display="inline-block" color="#454545" fontWeight="600" fontSize="24px">
          {whole}
        </Text>
        <Text ml="1" display="inline" color="#454545" fontWeight={500} verticalAlign="super" fontSize="14px">
          {numDecimal}
        </Text>
      </Box>
      <Button
        my="4"
        w="80%"
        visibility={showBtn ? "initial" : "hidden"}
        onClick={onLike}
      >
        I want this
      </Button>
    </Box>
  );
};
export default ProductCardDesktop;
