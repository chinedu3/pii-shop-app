import { Box, Flex, HStack, Text } from "@chakra-ui/react";
import { nanoid } from "nanoid";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import toast from 'react-hot-toast';
import { ProductType } from "utlis/constant";
import { getVariantId, getVariantGroup, getValidOptions } from "utlis/hooks";
import Select from 'components/Select';

type VariantDisplayProps = {
  product: ProductType;
};

const VariantProductId = (props: VariantDisplayProps) => {
  const { product } = props;
  const { allCriteriasWithIds, variantGroup } = getVariantGroup(product)
  const variantKeys = Object.keys(variantGroup);
  const history = useRouter();
  const currentVariant = product.variants.find(
    (currentProduct) => currentProduct.productId === product.productId
  );

  const handleSelection = (selected, variantName) => {
    const selection = {
      ...currentVariant?.criteria,
      ...selected,
    };
    const productId = getVariantId(
      selection, allCriteriasWithIds, variantName
    );
    if (productId) {
      const query: string = String(history.query.productId);
      const [, merchantName] = query.split("-");
      history.push(`/product/${productId}-${merchantName}`)
    } else {
      toast.dismiss();
      toast.error("Variant not avaliable")
    }

  };
  const getOptions = (variantName): any => {
    return getValidOptions(
      currentVariant?.criteria,
      variantName,
      allCriteriasWithIds,
      variantGroup
    ).map(item => ({ value: item.value, label: item.value, enabled: item.enabled }))
  }

  const [options, setOptions] = useState({});
  useEffect(() => {
    let newObj = {}
    variantKeys.forEach(varName => {
      newObj[varName] = getOptions(varName) || []
    });

    setOptions(newObj)
    // eslint-disable-next-line
  }, [])
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.data.enabled ? 'black' : 'gray',
    }),
  }
  return (
    <HStack>
      {variantKeys.map((variantName) => {
        return (
          <Box key={nanoid()}>
            <Text fontSize="14px" color="#6D707A" mt="2">
              {variantName}
            </Text>
            <Select
              styles={customStyles}
              fullWidth
              defaultValue={currentVariant?.criteria[variantName]}
              onChange={(option) => {
                handleSelection({ [variantName]: option.value }, variantName)
              }}
              options={options[variantName]}
              value={
                options[variantName]?.filter(option =>
                  option.label === currentVariant?.criteria[variantName])
              }
            />
          </Box>
        );
      })}
    </HStack>
  );
};

export default VariantProductId;
