import { AccordionItem, Flex, Heading, Accordion, AccordionButton, Box, AccordionIcon, AccordionPanel, Button } from '@chakra-ui/react'
import Rating from 'components/Rating'
import React from 'react'
import ReviewCard from 'components/ReviewCard';
import { nanoid } from 'nanoid';
const AccordionDisplay = (props) => {
  const { product, productReviews, loadMore, isFetching } = props;

  return (
    <Accordion allowToggle>
      <AccordionItem
        borderRadius="4px"
        borderTop="1px solid #D5D6D9"
      >
        <Heading as="h2" fontSize="14px" fontWeight="normal">
          <AccordionButton
            _focus={{
              boxShadow: "none"
            }}
          >
            <Box flex='1' textAlign='left' fontWeight={"bold"} fontSize="24px">
              Product Description
            </Box>
            <AccordionIcon color="#979696" />
          </AccordionButton>
        </Heading>
        <AccordionPanel pb={4}>
          <Box
            fontSize={"14px"}
            dangerouslySetInnerHTML={{ __html: product.description }} />
        </AccordionPanel>
      </AccordionItem>

      <AccordionItem
        mt="8px"
        borderTop="1px solid #D5D6D9"
        borderRadius="4px"
      >
        <h2>
          <AccordionButton
            _focus={{
              boxShadow: "none"
            }}
          >
            <Flex alignItems={"center"} flex='1' textAlign='left' fontWeight={"bold"} fontSize="24px">
              Reviews <Box fontSize={"14px"} ml="3">
                <Rating rating={product.rating} />
              </Box>
            </Flex>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel pb={4}>
          {
            productReviews.data.length === 0 ? <Box>Product does not have an reviews</Box> : null
          }
          {
            productReviews.data.map((review) => <ReviewCard key={nanoid()} review={review} color="#000" />)
          }
          <Flex justifyContent="center">
            {
              productReviews.data.length > 0 &&
              <Button w="sm" onClick={loadMore}
                disabled={productReviews.pagination.totalResults === productReviews.data.length}
                isLoading={isFetching}
              >Load More</Button>
            }
          </Flex>
        </AccordionPanel>
      </AccordionItem>
    </Accordion >
  )
}

export default AccordionDisplay