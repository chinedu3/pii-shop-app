import { Box, FormControl, Text, FormLabel, Switch } from "@chakra-ui/react";
import { nanoid } from "nanoid";
import React from "react";
import { ProductType } from 'utlis/constant';
import { createIdMapper } from "utlis/hooks";
type VariantDisplayProps = {
  product: ProductType;
};
const VariantDesktop = (props: VariantDisplayProps) => {
  const { product } = props;

  const [variantGroup] = createIdMapper(product);
  const variantKeys = Object.keys(variantGroup);
  const currentVariant = product.variants.find(
    (currentProduct) => currentProduct.productId === product.productId
  );
  return (
    <Box mb="8">
      {variantKeys.map((variantName) => (
        <Box key={nanoid()} p="4">
          <Text border="1px solid #C4C4C4" textAlign="center" p="3">
            {variantName}
          </Text>
          {
            <>
              {Array.from(variantGroup[variantName]).map((item: any) => (
                <FormControl
                  key={nanoid()}
                  borderBottom="1px solid #C4C4C4"
                  borderLeft="1px solid #C4C4C4"
                  borderRight="1px solid #C4C4C4"
                  p="3"
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <FormLabel htmlFor="email-alerts" mb="0">
                    {item}
                  </FormLabel>
                  <Switch

                    colorScheme="lime"
                    defaultChecked={currentVariant?.criteria[variantName] === item}

                  />
                </FormControl>
              ))}
            </>
          }
        </Box>
      ))}
    </Box>
  );
};

export default VariantDesktop;
