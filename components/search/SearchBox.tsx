import { InputGroup, Input, InputRightElement } from '@chakra-ui/react'
import React, { useState } from 'react'
import { AiOutlineSearch } from 'react-icons/ai'

type SearchBoxProps = {
  onChange: (event) => void
}

const SearchBox = (props: SearchBoxProps) => {
  const { onChange } = props;
  const [isEmpty, setIsEmpty] = useState(true)
  return (
    <InputGroup
      transition="all 500ms ease-in-out"
      background="#fff"
      width="100%"
      maxWidth="600px"
      margin="0 auto"
      borderRadius="4px"
      height="40px"
      alignItems="center"

    >
      <Input
        onChange={(event) => {
          const value = event.target.value
          setIsEmpty(value.length === 0)
          onChange(event)

        }}
        placeholder='Search'
        color="#000"
        type="search"
        _focus={{
          outline: 'none'
        }}
        fontSize="16px"
        width="95%"
        border="none"
        margin=" auto 0"
        marginLeft="1rem"
        height="90%"
        outline=" none"

      />
      {isEmpty && <InputRightElement h="100%" mr="3">
        <AiOutlineSearch color="#000" size="23px" />
      </InputRightElement>}
    </InputGroup>
  )
}

export default SearchBox