import React, { useEffect, useState } from "react";
import { Box, Flex, Text } from "@chakra-ui/react";
import { nanoid } from "nanoid";
import { getVariantId, getVariantGroup, getValidOptions } from "utlis/hooks";
import { ProductType } from "utlis/constant";
import { useRouter } from 'next/router'
import toast from 'react-hot-toast';
import Select from 'components/Select';


type VariantDisplayProps = {
  product: ProductType;
};
const VariantDisplay = (props: VariantDisplayProps) => {
  const { product } = props;
  const { allCriteriasWithIds, variantGroup } = getVariantGroup(product)
  const variantKeys = Object.keys(variantGroup);
  const history = useRouter();
  const currentVariant = product.variants.find(
    (currentProduct) => currentProduct.productId === product.productId
  );

  const handleSelection = (selected, variantName) => {
    const selection = {
      ...currentVariant?.criteria,
      ...selected,
    };
    const productId = getVariantId(
      selection, allCriteriasWithIds, variantName
    );
    if (productId) {
      const query: string = String(history.query.productId);
      const [, merchantName] = query.split("-");
      history.push(`/product/${productId}-${merchantName}`)
    } else {
      toast.dismiss();
      toast.error("Variant not avaliable")
    }

  };
  const getOptions = (variantName): any => {
    return getValidOptions(
      currentVariant?.criteria,
      variantName,
      allCriteriasWithIds,
      variantGroup
    ).map(item => ({ value: item.value, label: item.value, enabled: item.enabled }))
  }
  const [options, setOptions] = useState({});

  useEffect(() => {
    let newObj = {}
    variantKeys.forEach(varName => {
      newObj[varName] = getOptions(varName) || []
    });

    setOptions(newObj)
    // eslint-disable-next-line
  }, [])
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.data.enabled ? 'black' : 'gray',
    }),
  }
  return (
    <Box>
      {variantKeys.map((variantName) => {
        return (
          <Box key={nanoid()}>
            <Box>
              <Text fontSize="14px" color="#6D707A" mt="2">
                {variantName}
              </Text>
              <Flex flexWrap="wrap" w="100%">
                {variantName.toLowerCase() === "color" ? (
                  <Select
                    styles={customStyles}
                    fullWidth
                    defaultValue={currentVariant?.criteria[variantName]}
                    onChange={(option) => {
                      handleSelection({ [variantName]: option.value }, variantName)
                    }}
                    options={options[variantName]}
                    value={
                      options[variantName]?.filter(option =>
                        option.label === currentVariant?.criteria[variantName])
                    }
                  />
                ) : (
                  options[variantName]?.map((item: any) => {
                    console.log('item:', item)
                    return (
                      <Box
                        key={nanoid()}
                        border="1px solid #6D707A"
                        borderRadius="4px"
                        fontSize="14px"
                        p="0.25rem 0.5rem"
                        m="1"
                        bg={
                          !item.enabled ? "gray" : currentVariant?.criteria[variantName] === item.value
                            ? "#000"
                            : "#fff"
                        }
                        color={
                          !item.enabled ? "#fff" : currentVariant?.criteria[variantName] === item.value
                            ? "#fff"
                            : "#6D707A"
                        }
                        onClick={() => handleSelection({ [variantName]: item.value }, variantName)}
                      >
                        <Text>{item.value}</Text>
                      </Box>
                    );
                  })
                )}
              </Flex>
              <Box></Box>
            </Box>
          </Box>
        );
      })}
    </Box>
  );
};

export default VariantDisplay;
