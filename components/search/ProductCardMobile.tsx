import React, { useEffect, useState } from "react";
import {
  Box,
  Image,
  Flex,
  Text,
  useBoolean,
  Button,
  Grid
} from "@chakra-ui/react";
import { AiOutlineHeart, AiFillHeart, AiOutlineInfoCircle } from "react-icons/ai";
import { formatCurrency } from "utlis/hooks";
import { MERCHANT_LOGO } from 'utlis/constant'
import { addItemToCart, removeItemFromCart } from 'utlis/cart.utils';
import { useStoreData } from 'lib/store';
import toast from 'react-hot-toast';
import UpDownBtn from "./UpDownBtn";
import { useRouter } from 'next/router'
import VariantDisplay from './VariantDisplay';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import { nanoid } from 'nanoid';
import Rating from 'react-rating'
import { AiOutlineStar, AiTwotoneStar } from 'react-icons/ai';
import styled from '@emotion/styled';
import 'swiper/css';
import 'swiper/css/navigation';
type ProductCardMobileProps = {
  product: any
  shareProduct: (url) => void
  showDetails?: () => void
  canToggle?: boolean
  showReview?: () => void
}

const ProductCardMobile = (props: ProductCardMobileProps) => {
  const { product, shareProduct, canToggle = true, showReview } = props;
  const [showBtn, setshowBtn] = useBoolean();
  const [isLiked, setIsLiked] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [shareUrl, setshareUrl] = useState("")

  const history = useRouter();


  let fullPrice = "0";
  let oldPrice = "0";
  const productId = product.productId || product.id;
  const merchantName = product.merchant;
  useEffect(() => {
    const host = window.location.host;
    const protocol = `${window.location.protocol}//`;
    const shareUrl = `${protocol}${host}/product/${productId}-${merchantName}`
    setshareUrl(shareUrl);
    //eslint-disable-next-line
  }, [])
  const shareTitle = product.title;
  if (product.price) {
    const price = product.price.value / 100 || 0;
    let [whole] = price.toString().split(".");
    fullPrice = formatCurrency(whole, product.price.currency);
  }
  if (product.oldPrice) {
    const formerPrice = product?.oldPrice?.value / 100 || 0;
    oldPrice = formatCurrency(
      formerPrice ? formerPrice : 0,
      product.oldPrice.currency
    );
  }

  const [cartItems, updateCart] = useStoreData((state) => [
    state.cartItems,
    state.updateCart,
  ]);

  const handleLikeorDislike = () => {
    setIsLiked(!isLiked);
    if (!isLiked) {
      toast.success("Added to favourites");
      const newCart = addItemToCart(cartItems, product)
      updateCart(newCart)
    } else {
      toast("Removed from favourites", {
        icon: <AiOutlineInfoCircle />
      });
      const newCart = removeItemFromCart(cartItems, product);
      updateCart(newCart)

    }
  }
  const hasImageArray = product.images;
  return (
    <ProductCardMobile.Wrapper>
      <Box mt="4">
        <Box
          onClick={() =>
            history.pathname.includes("search") &&
            history.push(`/loading/product/${productId}-${merchantName}`)
          }
        >
          <Box position="relative" h="184px" overflow={"hidden"}>
            {
              hasImageArray ?
                <Swiper
                  modules={[Navigation]}
                  spaceBetween={0}
                  slidesPerView={1}
                  navigation
                  className="external-buttons"
                  loop
                >
                  {
                    product.images.map((image) =>
                      <SwiperSlide key={nanoid()}>
                        <Image
                          src={image}
                          alt="product name"
                          w="100%"
                          h="100%"
                          objectFit="scale-down"
                        /></SwiperSlide>
                    )
                  }
                </Swiper>
                : <Image
                  src={product.image}
                  alt="product name"
                  w="100%"
                  h="100%"
                  objectFit="scale-down"
                />
            }

            <Flex
              position="absolute"
              top="10px"
              right="10px"
              bg="white"
              w="32px"
              h="32px"
              boxShadow="0px 0px 2.97872px rgba(0, 0, 0, 0.25)"
              borderRadius="6px"
              alignItems={"center"}
              justifyContent={"center"}
              zIndex="5"
              _hover={{
                background: "gray.200",
                cursor: "pointer",
              }}
              onClick={(e) => {
                e.stopPropagation();
                handleLikeorDislike()
              }
              }
            >
              {isLiked ? (
                <AiFillHeart size="25px" />
              ) : (
                <AiOutlineHeart size="25px" />
              )}
            </Flex>
            <Flex
              position="absolute"
              bottom="10px"
              right="10px"
              zIndex="5"
              bg="white"
              w="32px"
              h="32px"
              boxShadow="0px 0px 2.97872px rgba(0, 0, 0, 0.25)"
              borderRadius="6px"
              alignItems={"center"}
              justifyContent={"center"}
              onClick={(e) => {
                e.stopPropagation();
                if (navigator.share) {
                  navigator.share({
                    title: shareTitle,
                    url: shareUrl
                  })
                } else {
                  shareProduct(shareUrl)
                }
              }}
            >
              <Image src="https://res.cloudinary.com/pii/image/upload/v1647979180/share_gfquko.svg"
                alt="share icon"
                width="25px" />
            </Flex>
          </Box>
          <Grid
            templateColumns="38px auto auto"
            gap="0.5rem"
            w="100%"
            py="4"
            px="0.5rem"
          >
            <Flex>
              <Flex
                borderRadius="50%"
                border="1px solid #E7E8E9"
                bg="#fff"
                alignItems="center"
                justifyContent={"center"}
                w="38px"
                h="38px"
              >
                <Image
                  src={MERCHANT_LOGO[product.merchant]}
                  w="32px"
                  alt="merchant logo"
                />
              </Flex>
            </Flex>
            <Text
              w="100%"
              fontSize="14px"
              noOfLines={canToggle ? 2 : 12}
              h={canToggle ? "40px" : "100%"}
              textTransform="capitalize"
            >
              {product.title}
            </Text>

            <Box>
              <Text
                textAlign="right"
                fontWeight={700}>{fullPrice}</Text>
              {oldPrice !== "0" && (
                <Text
                  fontWeight="medium"
                  fontSize={"12px"}
                  textAlign="right"
                  textDecoration={"line-through"}
                  color="#979696"
                >
                  {oldPrice}
                </Text>
              )}
            </Box>
          </Grid>
          {!canToggle && <Flex ml="54px"
            onClick={(e) => {
              e.stopPropagation();
              if (showReview) {
                showReview()
              }
            }}
          >
            <Rating
              readonly
              initialRating={product.rating}
              emptySymbol={<AiOutlineStar />}
              fullSymbol={<AiTwotoneStar />}
            />
            <Text
              ml="1"
              color="#1976D2"
              fontSize="14px"
              textDecoration="underline"
            > ({product.rating}) See Reviews</Text>
          </Flex>}
          <Flex justifyContent="center" borderBottom="1px solid #F5F5F6">
          </Flex>
        </Box>
        {!canToggle && <Box>
          <Box p="0.5rem 1rem">
            {!canToggle && <Box>
              <VariantDisplay product={product} />
            </Box>}
            <Flex mt="4">
              <Box>
                <Text fontSize={"14px"} color="#6D707A">
                  Quantity:
                </Text>
                <UpDownBtn
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                  onIncrement={() => setQuantity(quantity + 1)}
                  onDecrement={() => {
                    if (quantity > 0) {
                      setQuantity(quantity - 1);
                    }
                  }}
                />
              </Box>

            </Flex>

            <Button
              isFullWidth
              variant="secondary"
              mt="4"
              onClick={() => handleLikeorDislike()}
              style={{
                background: isLiked ? "black" : "white",
                color: isLiked ? "white" : "black",
                boxShadow: "0px 1px 3px rgb(0 0 0 / 20%), 0px 0px 8px rgb(0 0 0 / 6%)"
              }}
              _hover={{
                background: isLiked ? "black" : "white",
              }}
            >
              I want this
            </Button>
          </Box>
        </Box>}
      </Box>
    </ProductCardMobile.Wrapper>
  );
};
ProductCardMobile.Wrapper = styled.div`
  .swiper-button-prev, .swiper-button-next {
	 top: 45%;
	 width: 40px;
	 height: 40px;
	 border-radius: 50%;
	 color: black;
	 font-weight: 700;
	 outline: 0;
	 transition: background-color 0.2s ease, color 0.2s ease;
}
 .swiper-button-prev::after, .swiper-button-next::after {
	 font-size: 10px;
   color:#979696
}
 .swiper-button-prev:after {
	 position: relative;
	 left: -1px;
}
 .swiper-button-next:after {
	 position: relative;
	 left: 1px;
}
 .swiper-button-prev, .swiper-container-rtl .swiper-button-next {
	 left: 10px;
	 right: auto;
}
 .swiper-button-next, .swiper-container-rtl .swiper-button-prev {
	 right: 10px;
	 left: auto;
}
 .swiper-button-prev.swiper-button-disabled, .swiper-button-next.swiper-button-disabled {
	 opacity: 0;
	 cursor: auto;
	 pointer-events: none;
}
 

`
export default ProductCardMobile;
