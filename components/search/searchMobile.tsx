import {
  Box,
  Text,
  Heading,
  Container,
  useDisclosure,
  useBoolean,
  Flex
} from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import _ from "lodash";
import NavBar from "components/NavBar";
import BottomNav from "components/BottomNav";
import ProductCardMobile from "components/search/ProductCardMobile";
import ShareModal from "components/search/ShareModal";
import { useRouter } from 'next/router'

const SearchMobile = ({ data }) => {
  const [searchValue, setSearchValue] = useState<any>("");
  const [shareUrl, setShareUrl] = useState("");
  const { onOpen, isOpen, onClose } = useDisclosure();
  const handleSharing = (url) => {
    setShareUrl(url);
    onOpen();
  };
  const history = useRouter();
  useEffect(() => {
    const searchTerm = history.query.searchTerm || ""

    setSearchValue(searchTerm)

  }, [history.query.searchTerm])


  const [isProdDescriptionOpen, setisProdDescriptionOpen] = useBoolean();

  return (
    <Box position={"relative"} transition="all 2s ease">
      <ShareModal isOpen={isOpen} onClose={onClose} shareUrl={shareUrl} />
      <NavBar />
      <>
        <Box
          display={{ base: "none", md: "block" }}
          m="3rem auto"
          textAlign="center"
        >
          <Text display="inline" fontWeight="bold">
            {data.pagination.totalResults.toLocaleString()}{" "}
          </Text>
          <Text display="inline-block"> Search result for:</Text>
          <Heading fontWeight="400" mt="1" textTransform="capitalize">
            {searchValue}{" "}
          </Heading>
        </Box>
        <Container
          p={{ base: 0, md: "initial" }}
          position={"relative"}
          transition="all 0.6s ease"
        >
          <Flex
            height="calc(100vh - 110px)"
            flexDir="column"
            overflow={isProdDescriptionOpen ? "hidden" : "scroll"}
          >
            {data.data.map((product, index) =>
              <ProductCardMobile
                key={index}
                product={product}
                shareProduct={handleSharing}
                showDetails={setisProdDescriptionOpen.toggle}
              />

            )}
          </Flex>
        </Container>
      </>

      {data &&
        <BottomNav />
      }
    </Box>
  );
};

export default SearchMobile;
