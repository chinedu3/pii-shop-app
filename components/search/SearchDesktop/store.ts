import { BasicProductDefault, BasicProductType } from 'utlis/constant';
import create from 'zustand';
import { devtools } from 'zustand/middleware';

const selectedProduct = (set) => ({
  product: BasicProductDefault,
  updateProduct: (product: BasicProductType) => {
    set({ product });
  },
});

export const useSelectedProduct = create(devtools(selectedProduct));
