import { Image, Button, Drawer, DrawerOverlay, DrawerContent, Text, DrawerFooter, Flex, Box, Grid } from '@chakra-ui/react'
import { BasicProductType, MERCHANT_LOGO } from 'utlis/constant'
import { AiOutlineClose } from 'react-icons/ai'
import { nanoid } from 'nanoid'
import { useStoreData } from 'lib/store'
import { getCartTotal } from 'utlis/cart.utils'
import { formatCurrency } from 'utlis/hooks'
type CheckoutProps = {
  isOpen: boolean
  onClose: () => void
}
interface CartProduct extends BasicProductType {
  quantity: number
}
const Checkout = (props: CheckoutProps) => {
  const { isOpen, onClose } = props;
  const [cartItems, updateCart] = useStoreData((state) => [
    state.cartItems,
    state.updateCart,
  ]);

  return (
    <>
      <Drawer
        isOpen={isOpen}
        placement='right'
        onClose={onClose}
        size="sm"
      >
        <DrawerOverlay />
        <DrawerContent>
          <Flex p="4" alignItems="center" >
            <Button
              variant="unstyled"
              onClick={onClose}
            ><AiOutlineClose size={25} /></Button>
            <Text w="100%" textAlign="center" fontSize="18px" fontWeight="500">My Favorites</Text>
          </Flex>

          <Flex p="10px 1rem" justifyContent="space-between" bg="#000" color="#fff">

            <Text>Item Total: {cartItems.length} </Text>
            <Text>Total Amount: <Text fontWeight="bold" display="inline">{formatCurrency(getCartTotal(cartItems) / 100, "NGN")}   </Text> </Text>
          </Flex>
          <Box p="4">
            {
              cartItems.map((product: CartProduct) => {
                const price = product?.price.value / 100 || 0;
                let [whole] = price.toString().split(".");
                whole = formatCurrency(whole, product.price.currency);

                return <Grid
                  key={nanoid()}
                  templateColumns="117px auto auto"
                  gap="0.7rem"
                  mb="4"
                >
                  <Image
                    src={product.image}
                    alt="product preview"
                    w="117px"
                    h="97px"
                  />
                  <Flex
                    flexDir="column"
                    justifyContent="space-between"
                  >
                    <Text fontSize="14px"> {product.title} </Text>
                    <Text fontSize="13px" mb="4" color="#979696">Qty: {product.quantity} | Colour: Blue | Size: 23cm</Text>
                  </Flex>
                  <Flex
                    flexDir="column"
                    justifyContent="space-between"
                  >

                    <Text fontWeight="bold"> {whole} </Text>
                    <Image
                      src={MERCHANT_LOGO[product.merchant]}
                      alt="merchanticon"
                      mb="4"
                      w="63px"
                      h="19px"

                    />
                  </Flex>
                </Grid>
              })
            }

          </Box>
          <DrawerFooter bg="#F5F5F6" position="absolute" bottom={0} w="100%">
            <Button bg="#000" color="#fff" isFullWidth>Checkout Now</Button>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  )
}

export default Checkout;