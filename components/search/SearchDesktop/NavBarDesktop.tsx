import { Box, Avatar, Flex, HStack } from "@chakra-ui/react";
import React, { useState } from "react";
import NextImage from "next/image";
import { GiHamburgerMenu } from "react-icons/gi";
import piiLogo from "assets/piilogo.svg";
import notification from "assets/notification.svg";
import heart from "assets/heart.svg";
import SearchBox from "components/search/SearchBox";
import _ from "lodash";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import axios from 'axios';
import { productRequest } from 'services';
import { convertParamsToString } from 'services/helper';
import { BASE_URL } from 'utlis/constant';
import toast from 'react-hot-toast';
import { useStoreData } from 'lib/store';

type NavBarDesktop = {
  refetch?: () => void
}
const NavBarDesktop = () => {
  const history = useRouter();
  const [searchTerm, setSearchTerm] = useState("")
  const handleChange = _.debounce(function (event) {
    const searchTerm = event.target.value;
    if (searchTerm) {
      setSearchTerm(searchTerm);
      history.push(`/search/${searchTerm}`);
    }
  }, 1000);

  const getProducts = async (params: any) => {
    const { queryKey } = params;

    const url = convertParamsToString(productRequest.SEARCH, {
      search: queryKey[1],
      page: "1",
    });
    const response = await axios.get(BASE_URL + url);
    return response.data;
  };

  const [udpateResult, updateLoading] = useStoreData(state => [state.updateResult, state.updateLoading])

  const updateState = (res) => {
    udpateResult({
      isLoading: false,
      data: res
    });
    updateLoading(false)
  }

  useQuery(
    ["SEARCH_KEY--", searchTerm], getProducts,
    {
      enabled: !!searchTerm,
      onSuccess: (res) => updateState(res),
      onError: () => toast.error("An error occured"),
    }
  );

  return (
    <Flex
      as="nav"
      bg="#000"
      color="#fff"
      top="0"
      position="sticky"
      zIndex="7"
      alignItems="center"
      p="3"
      justifyContent="space-between"
    >
      <Flex>
        <HStack alignItems="center" spacing="10px" w="100%">
          <Box mr="3">
            <GiHamburgerMenu size={23} />
          </Box>
          <NextImage src={piiLogo} height={34} width={150} alt="this is icon" />
        </HStack>
      </Flex>
      <SearchBox onChange={handleChange} />
      <Flex>
        <NextImage
          src={notification}
          height={25}
          width={25}
          alt="this is icon"
        />
        <Flex mx="3">
          <NextImage src={heart} height={25} width={25} alt="this is icon" />
        </Flex>
        <Avatar w="25px" h="25px" />
      </Flex>
    </Flex>
  );
};

export default NavBarDesktop;
