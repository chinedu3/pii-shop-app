import React from "react";
import { Box, Button, Grid, useDisclosure } from "@chakra-ui/react";
import { nanoid } from "nanoid";
import ProductCardDesktop from "../ProductCardDesktop";
import AddProduct from './AddProductDrawer';
import Checkout from './CheckoutDrawer';
import NavBar from './NavBarDesktop'

type SearchDesktopProps = {
  products: {
    data: any[],
    pagination: any
  },
}
const SearchDesktop = (props: SearchDesktopProps) => {
  const { products } = props;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { isOpen: isOpenCheckout, onOpen: onOpenCheckout, onClose: onCloseCheckout } = useDisclosure();
  const handleLike = () => {
    onOpen();
  }
  return (
    <Box>
      <NavBar />
      <Grid templateColumns="repeat(4, 1fr)" gridGap="4" m="8">
        {products.data.map((product) => (
          <ProductCardDesktop
            key={nanoid()}
            product={product}
            handleLike={handleLike}
          />
        ))}
      </Grid>
      <AddProduct isOpen={isOpen} onClose={onClose} openCheckout={onOpenCheckout} />
      <Checkout isOpen={isOpenCheckout} onClose={onCloseCheckout} />
      <Box bg="orange.300">Pagination.....</Box>
    </Box>
  );
};

export default SearchDesktop;
