import { Image, Heading, Button, Drawer, Switch, DrawerOverlay, DrawerContent, Text, DrawerFooter, Flex, Box } from '@chakra-ui/react'
import { BASE_URL, MERCHANT_LOGO } from 'utlis/constant'
import { AiOutlineClose } from 'react-icons/ai'
import { useSelectedProduct } from './store'
import { addItemToCart } from 'utlis/cart.utils';
import { formatCurrency } from 'utlis/hooks'
import { useStoreData } from 'lib/store'
import { productRequest } from 'services'
import { convertParamsToString } from 'services/helper'
import axios from 'axios'
import { useQuery } from 'react-query'
import VariantDesktop from '../VariantDesktop'
import UpDownBtn from '../UpDownBtn';
import { useState } from 'react';
type AddProductDrawerProps = {
  isOpen: boolean
  onClose: () => void
  openCheckout: () => void

}

const AddProductDrawer = (props: AddProductDrawerProps) => {
  const { isOpen, onClose, openCheckout } = props;
  const [product] = useSelectedProduct(state => [state.product])
  const [cartItems, updateCart] = useStoreData((state) => [
    state.cartItems,
    state.updateCart,
  ]);
  const price = product?.price.value / 100 || 0;
  let [whole] = price.toString().split(".");
  whole = formatCurrency(whole, product.price.currency);
  const saveItem = () => {
    const newCart = addItemToCart(cartItems, product)
    updateCart(newCart);
    openCheckout();
    onClose();
  }

  const [quantity, setQuantity] = useState(1)
  const getProductInfo = async () => {
    const url = convertParamsToString(productRequest.PRODUCT_INFO, { productId: product.productId, merchantName: product.merchant })
    const response = await axios.get(BASE_URL + url);
    return response.data.data;
  };
  const { isSuccess, data } = useQuery("product-info", getProductInfo, {
    enabled: isOpen
  })
  return (
    <>
      <Drawer
        isOpen={isOpen}
        onClose={onClose}
        placement='right'
        size="sm"
      >
        <DrawerOverlay />
        <DrawerContent>
          <Button
            variant="unstyled"
            onClick={onClose}
            ml="4"
            mt="4"
          ><AiOutlineClose size={25} />
          </Button>

          <Box textAlign="center">
            <Image
              src={product.image}
              alt="kitten"
              width="356px"
              height="294px"
              m="auto"
              objectFit="fill"
            />
            <Heading as="h1" fontSize="28px" mt="4" fontWeight="500"
              isTruncated
              p="0 2rem"
            >{product.title} </Heading>
            <Image
              src={MERCHANT_LOGO[product.merchant]}
              alt="merchanticon"
              m="1rem auto"
            />
            <Text fontSize="24px" fontWeight="700">{whole}  </Text>
            {/* <Text color="#6D707A">Qty: 1 | Size: 23cm | Colour: Blue</Text> */}
          </Box>
          <Flex
            mt="4"
            p="18px 40px"
            justifyContent="space-between"
            borderTop="1px solid #C4C4C4" borderBottom="1px solid #C4C4C4">
            <Text>Quantity</Text>
            <UpDownBtn
              value={quantity}
              onChange={(e) => setQuantity(e.target.value)}
              onIncrement={() => setQuantity(quantity + 1)}
              onDecrement={() => {
                if (quantity > 0) {
                  setQuantity(quantity - 1);
                }
              }}
            />
          </Flex>

          <Box overflow={"scroll"} mb="8">
            {isSuccess && <VariantDesktop product={data} />}
          </Box>

          <DrawerFooter bg="#F5F5F6" position="absolute" bottom={0} w="100%">
            <Button onClick={saveItem} bg="#000" color="#fff" isFullWidth>Save</Button>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  )
}

export default AddProductDrawer;