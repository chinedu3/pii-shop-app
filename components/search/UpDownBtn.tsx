import { Flex, Button, Input, FlexProps } from '@chakra-ui/react';
import React from 'react';

type UpDownBtnProps = {
  value: any;
  onChange: (target) => void;
  onIncrement: () => void;
  onDecrement: () => void;
};
function StyledButton({ children, ...props }) {
  return (
    <Button
      borderRadius={0}
      h='100%'
      bg='transparent'
      height='100%'
      _hover={{ background: 'transparent' }}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
    >
      {children}
    </Button>
  );
}

const UpDownBtn = (props: UpDownBtnProps) => {
  const { value, onChange, onDecrement, onIncrement } = props;
  return (
    <Flex
      border='1px solid #979696'
      w='max-content'
      borderRadius='30px'
      height='30px'
      alignItems='center'
    // eslint-disable-next-line react/jsx-props-no-spreading
    // {...props}
    >
      <StyledButton
        onClick={(e) => {
          e.stopPropagation();
          typeof onDecrement === 'function' && onDecrement();
        }}
      >
        -
      </StyledButton>
      <Input
        border='none'
        borderRadius='none'
        min={0}
        type='number'
        value={value}
        onClick={(e) => e.stopPropagation()}
        onChange={onChange}
        alignSelf='center'
        padding='0'
        width='20px'
        textAlign='center'
        fontSize={"14px"}
        _focus={{ background: 'none' }}
      />
      <StyledButton
        onClick={(e) => {
          e.stopPropagation();
          typeof onIncrement === 'function' && onIncrement();
        }}
      >
        +
      </StyledButton>
    </Flex >
  );
};

export default UpDownBtn;
