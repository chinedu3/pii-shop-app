import Head from 'next/head';
import PropTypes from "prop-types";

type MetaDecoratorProps = {
  title: string;
  description: string;
  imageUrl: string;
  imageAlt?: string;
};
const MetaDecorator = (props: MetaDecoratorProps) => {
  const {
    title,
    description,
    imageUrl = "https://i.picsum.photos/id/699/200/300.jpg?hmac=s68cvOJXxl4ZvaOM6PpveL8klBiaViC9Nbi02oETt5k",
    imageAlt,
  } = props;
  return (

    <Head>
      <title>{title}</title>
      <meta property="og:title" content={title} />
      <meta name="description" content={description} />
      <meta property="og:description" content={description} />

      <meta property="og:image" content={imageUrl} />


      {/* <!-- Twitter --> */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={description} />
      <meta property="twitter:image" content={imageUrl}></meta>
      <meta name="twitter:image:alt" content={imageAlt} />
      <meta name="twitter:site" content="@chinedu_knight" />

      <meta
        name="keywords"
        content=""
      />
      <meta
        name="msapplication-TileImage"
        content={imageUrl}
      ></meta>
    </Head>
  );
};

MetaDecorator.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  imageUrl: PropTypes.string,
  imageAlt: PropTypes.string,
};

MetaDecorator.defaultProps = {
  title: "pii-shop.com",
  description: "all in one shopping experience ",
  keywords: "product, price",
  imageAlt: "product image",
};

export default MetaDecorator;
