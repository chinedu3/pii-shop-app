import { Flex, Box, Input, useBoolean, Text, HStack, Image } from '@chakra-ui/react'
// import SearchBox from './search/SearchBox'
import React from 'react'
import { AiOutlineSearch } from 'react-icons/ai'
// import { BsCartFill, BsArrowLeft } from 'react-icons/bs'
// import { useStoreData } from 'lib/store'
import { PII_LOGO } from 'utlis/constant'
// const NavBar = (props) => {
//   const [cartItems, updateCart] = useStoreData((state) => [
//     state.cartItems,
//     state.updateCart,
//   ]);
//   return (
//     <Box>
//       <Flex
//         as="nav"
//         bg="black"
//         justifyContent="space-between"
//         alignItems="center"
//         color="#fff"
//         // px="2"
//         // py="3"
//         // pl="40px"
//         position="sticky"
//         top="0"
//         zIndex="5"
//       >
//         <Flex alignItems="center">
//           <Image src={PII_LOGO} alt="pii sub image" />
//         </Flex>
//         <Box w="100%" display={{
//           base: "none",
//           lg: "initial"
//         }}>
//           <SearchBox
//             onChange={props.handleChange}
//           />
//         </Box>
//         <HStack alignItems="center" spacing='4'>
//           <Flex alignItems="center" display="none">
//             <BsCartFill size={25} />
//             <Text fontSize="sm" color="#979696" fontWeight="bold" ml="1">
//               2</Text>
//           </Flex>
//           {/* <Flex alignItems="center">
//             <AiFillHeart size={25} />
//             <Text fontSize="sm" color="#979696" fontWeight="bold" ml="1">

//               {cartItems.length}</Text>
//           </Flex> */}
//           <Flex
//             _hover={{
//               cursor: "pointer"
//             }}
//             onClick={setIsSearchVisible.toggle}
//           ><AiOutlineSearch size={25} />
//           </Flex>
//           {/* <Box alignItems="center">
//             <Avatar src="https://picsum.photos/200/300"
//               width="25px"
//               height="25px"
//               border="2px solid #fff"
//             />
//           </Box> */}
//         </HStack>
//       </Flex>
//       <Flex
//         transition="all 0.6s ease"
//         overflow="hidden"
//         h={isSearchVisible ? "51px" : "0"}
//       >
//         <Flex w="100%"
//           borderBottom="5px solid #F0F0F0"
//           p="8px"
//           alignItems={"center"}
//         >
//           <Box>
//             <BsArrowLeft size={25} color="#C4C4C4" />
//           </Box>

//           <Input
//             type="search"
//             h="30px"
//             placeholder="Search for sdsd a product"
//             fontSize="14px"
//             ml="2"
//             bg="#F0F0F0"
//             outline="none"
//             _focus={{
//               boxShadow: "none"
//             }}
//             onChange={props.handleChange}
//           />
//         </Flex>
//       </Flex>
//     </Box>
//   )
// }

// export default NavBar


const NavBarMobile = () => {
  const [isSearchVisible, setIsSearchVisible] = useBoolean(true)
  return (
    <Flex
      as="nav"
      bg="black"
      justifyContent="space-between"
      alignItems="center"
      color="#fff"
      p="15px 16px"
      position="sticky"
      top="0"
      zIndex="5"
    >
      <Image src={PII_LOGO} alt="pii sub image" />
      <Flex
        _hover={{
          cursor: "pointer"
        }}
        onClick={setIsSearchVisible.toggle}
      ><AiOutlineSearch size={25} /></Flex>
    </Flex>
  )
}

export default NavBarMobile