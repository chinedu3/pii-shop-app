import React, { useState } from "react";
import NavBarDesktop from "components/search/SearchDesktop/NavBarDesktop";
import {
  Flex,
  Box,
  Button,
  Image,
  HStack,
  Text,
  Heading,
  VStack,
  Container
} from "@chakra-ui/react";
import { nanoid } from "nanoid";
import Rating from "components/Rating";
import { MERCHANT_LOGO, ProductType } from "utlis/constant";
import Accordion from "components/search/Accordion";
import { formatCurrency } from "utlis/hooks";
import UpDownBtn from 'components/search/UpDownBtn';
import VariantProductId from 'components/search/VariantProductId';

type DesktopProdViewProps = {
  product: ProductType;
  productReviews: any;
  loadMore: any;
  isFetching: boolean;
};

const DesktopProdView = (props: DesktopProdViewProps) => {
  const { product, productReviews, loadMore, isFetching } = props;
  const [quantity, setQuantity] = useState(1)
  let tempFullPrice = "0";
  let tempOldPrice = "0";
  let numDecimal: string | number = "0";
  if (product.price) {
    const price = product.price.value / 100 || 0;
    let [whole, decimal] = price.toString().split(".");
    tempFullPrice = formatCurrency(whole, product.price.currency);
    numDecimal = Number(decimal);
    numDecimal = !numDecimal
      ? "00"
      : +numDecimal < 10
        ? numDecimal * 10
        : numDecimal;
  }
  if (product.oldPrice) {
    const formerPrice = product?.oldPrice?.value / 100 || 0;
    tempOldPrice = formatCurrency(
      formerPrice ? formerPrice : 0,
      product.oldPrice.currency
    );
  }


  const [currentImage, setcurrentImage] = useState(product.image);
  const [imageNo, setImageNo] = useState(1);
  const productInfo = {
    name: product.title,
    merchant: product.merchant,
    otherImages: product.images,
    rating: product.rating,
    oldPrice: tempOldPrice,
    price: tempFullPrice,
    isAvaliable: product.in_stock
  };
  const { name, merchant, otherImages, rating, oldPrice, price, isAvaliable } = productInfo;
  return (
    <Box>
      <NavBarDesktop />
      <Container
        maxW="container.xl"
        p={{ base: "0", lg: "40px" }}
      >
        <Flex>
          <VStack w="592px">
            <Box
              alignSelf="flex-start"
              background="#000000"
              borderRadius="34px"
              color="#fff"
              p="9px 17px"
              fontWeight={700}
            >
              {imageNo}/{otherImages.length}{" "}
            </Box>
            <Image src={currentImage} alt="prod view"
              objectFit={"contain"}
              h="304px" w="288px" />
            <HStack mt="3" flexWrap="wrap">
              {otherImages.map((image, index) => (
                <Button
                  key={nanoid()}
                  onClick={() => {
                    setcurrentImage(image);
                    setImageNo(index + 1);
                  }}
                  variant="unstyled"
                  border="1px solid #c4c4c4"
                  h="58px"
                  w="63px"
                  p="1"
                  m="1"
                >
                  {" "}
                  <Image src={image} objectFit={"contain"} alt="prod view" w="100%" h="100%" />
                </Button>
              ))}
            </HStack>
          </VStack>
          <Box>
            <Heading
              as="h1"
              fontSize="32px"
              fontWeight={500}
              maxW="615px"
              mb="3"
            >
              {name}{" "}
            </Heading>
            <Flex>
              {" "}
              <Rating rating={rating} />
              <Text
                ml="1"
                color="#1976D2"
                fontSize="14px"
                textDecoration="underline"
              >
                {" "}
                ({rating}) rating{" "}
              </Text>
              <Image
                src={MERCHANT_LOGO[merchant]}
                ml="2"
                objectFit="cover"
                alt="merchant logo"
              />
            </Flex>
            {+oldPrice > 0 ? <Flex>
              <Text color="#6D707A" fontSize="14px">
                Last Price:
              </Text>
              <Text
                fontSize={"14px"}
                ml="1"
                color="#6D707A"
                fontWeight={500}
                textDecoration="line-through"
              >
                {oldPrice}{" "}
              </Text>
            </Flex> : null}
            <Text fontSize="sm" color="#6D707A">
              Price:
            </Text>
            <Text fontWeight={"bold"} display="inline" fontSize="24px">
              {price}
            </Text>
            <Text
              fontWeight={"medium"}
              display="inline"
              fontSize="sm"
              verticalAlign="super"
            >
              {" "}
              {numDecimal}
            </Text>
            <Box>
              <Box>
                <Text fontSize="14px" color="#6D707A"  >Quantity</Text>
                <UpDownBtn
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                  onIncrement={() => setQuantity(quantity + 1)}
                  onDecrement={() => {
                    if (quantity > 0) {
                      setQuantity(quantity - 1);
                    }
                  }}
                />
              </Box>
              <VariantProductId product={product} />
            </Box>
            {isAvaliable ? <Button
              display="block"
              bg="#fff"
              border="2px solid #454545"
              borderRadius="30px"
              w="sm"
              mt="4"
            >
              Save
            </Button> :
              <Text mt="2" color="orangered" fontWeight={"bold"}>Currently Unavaliable</Text>
            }

          </Box>
        </Flex>
        <Box mt="4">
          <Accordion
            product={product}
            productReviews={productReviews}
            loadMore={loadMore}
            isFetching={isFetching}
          />
        </Box>
      </Container>
    </Box >
  );
};

export default DesktopProdView;
