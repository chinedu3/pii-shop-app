import { Box, Flex, CloseButton, Text, Divider } from '@chakra-ui/react';
import BottomNav from 'components/BottomNav';
import Loader from 'components/Loader';
import MetaDecorator from 'components/MetaDecorator';
import NavBar from 'components/NavBar';
import ReviewCard from 'components/ReviewCard';
import ProductCardMobile from 'components/search/ProductCardMobile';
import { nanoid } from 'nanoid';
import React from 'react'

type MobilePageTypes = {
  product: any,
  showReview: boolean,
  setShowReview: { readonly on: () => void; readonly off: () => void; readonly toggle: () => void; },
  productReviews: { data: never[]; pagination: { current: number; perPage: number; totalPages: number; totalResults: number; }; },
  lastBookElementRef: (node: any) => void,
  isFetching: boolean
}
const MobilePage = (props: MobilePageTypes) => {
  const { product, showReview, setShowReview, productReviews, lastBookElementRef, isFetching } = props;
  return <Box position="relative">
    <MetaDecorator
      title={product.title}
      imageUrl={product.image} />
    <NavBar />
    <Box
      h="calc(100vh - 100px)"
      overflow={showReview ? "hidden" : "scroll"}
    >
      <ProductCardMobile
        product={product}
        shareProduct={(url) => console.log("see me", url)}
        canToggle={false}
        showReview={setShowReview.toggle} />
      <Box p="4" mt="4"
        borderTop="4px solid  #F0F0F0"
      >
        <Text fontSize="14px"
          color="#979696"
          fontWeight={500}
          textTransform="uppercase"
          mb="4"
        >Description</Text>
        <Box fontSize={"14px"}
          pb="12"
          overflow="scroll"
          dangerouslySetInnerHTML={{ __html: product.description }} />
      </Box>
    </Box>
    <BottomNav />
    <Box
      position={"absolute"}
      bottom="0"
      background="#FFFFFF"
      w="100%"
      h={showReview ? "50vh" : "0"}
      overflow={"hidden"}
      transition="height 0.6s ease"
      zIndex="41"
    >
      <Flex
        boxShadow="0px 0px 12px rgba(0, 0, 0, 0.15)"
        borderRadius="20px 20px 0px 0px"
        alignItems={"center"}
        justifyContent={"space-between"}
        p="8px 1rem"
        bg="#fff"
        borderTop="1px solid rgba(0, 0, 0, 0.15)"
      >
        <Text fontWeight={500}>Reviews</Text>
        <CloseButton
          onClick={setShowReview.toggle}
          _focus={{
            boxShadow: "none"
          }} />
      </Flex>
      <Divider />
      <Box h="100%" bg="#fff" p="4" overflow={"scroll"}
        pt="4"
        pb="5rem"
      >
        {productReviews.data.length === 0 ? <div>No Reviews currently </div> : productReviews.data.map((review, index) => {
          if (productReviews.data.length === index + 1) {
            return <div ref={lastBookElementRef} key={nanoid()}> <ReviewCard review={review} /></div>;
          } else {
            return <ReviewCard key={nanoid()} review={review} />;
          }
        }
        )}
        {isFetching &&
          <Loader />}
      </Box>
    </Box>
  </Box>;
}
export default MobilePage