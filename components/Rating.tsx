import React from 'react'
import { AiOutlineStar, AiTwotoneStar } from 'react-icons/ai';
import StarRating from 'react-rating'

type RatingProps = {
  rating: number,
  color?: string
}

const Rating = (props: RatingProps) => {
  const { rating, color = "#000" } = props
  return (
    <StarRating
      readonly
      initialRating={rating}
      emptySymbol={<AiOutlineStar color={color} />}
      fullSymbol={<AiTwotoneStar color={color} />}
    />
  )
}

export default Rating