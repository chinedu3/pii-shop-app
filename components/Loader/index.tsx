import React from 'react';
import { Spinner, Center, Text } from '@chakra-ui/react'

type LoaderProps = {
  loadingText?: string
  isFullHeight?: boolean
}

const Loader = (props: LoaderProps) => {
  const { loadingText, isFullHeight } = props
  return (
    <Center flexDir={"column"} h={isFullHeight ? "100vh" : "20vh"} >
      <Spinner
        thickness='4px'
        speed='0.95s'
        emptyColor='#cccccc'
        color='black'
        size='xl'
      />
      {
        loadingText &&
        <Text mt="4"
          color="#C4C4C4"
          fontWeight={400}
        > {loadingText}</Text>
      }
    </Center>
  );
};

export default Loader;
