import { Flex } from '@chakra-ui/react';
import React from 'react'
import { AiOutlineHeart, AiFillHeart, AiOutlineInfoCircle } from "react-icons/ai";

type LikeIconProps = {
  isLiked: boolean;
  handleLikeorDislike: () => void;
}

const LikeIcon = (props: LikeIconProps) => {
  const { isLiked, handleLikeorDislike } = props
  return (
    <Flex
      position="absolute"
      top="10px"
      right="10px"
      bg="white"
      w="32px"
      h="32px"
      boxShadow="0px 0px 2.97872px rgba(0, 0, 0, 0.25)"
      borderRadius="6px"
      alignItems={"center"}
      justifyContent={"center"}
      zIndex="5"
      _hover={{
        background: "gray.200",
        cursor: "pointer",
      }}
      onClick={(e) => {
        e.stopPropagation();
        handleLikeorDislike()
      }
      }
    >
      {isLiked ? (
        <AiFillHeart size="25px" />
      ) : (
        <AiOutlineHeart size="25px" />
      )}
    </Flex>
  )
}

export default LikeIcon