import { Avatar, Box, Flex, Text } from '@chakra-ui/react';
import React from 'react'
import { AiOutlineStar, AiTwotoneStar } from 'react-icons/ai';
import Rating from 'components/Rating';
type ReviewCardProps = {
  review: {
    reviewerName: string,
    body: string,
    datePublished: string,
    rating: number
  }
  color?: string
}

const ReviewCard = (props: ReviewCardProps) => {
  const { review, color = "#979696" } = props;
  const reviewInfo = {
    name: review.reviewerName,
    body: review.body,
    date: review.datePublished,
    rating: review.rating
  }

  const { name, body, date, rating } = reviewInfo;
  return <Flex mb="4">
    <Avatar
      w="16px"
      h="16px"
      name={name}

    />
    <Box ml="8px">
      <Box>
        <Text
          fontSize="12px"
          color="#979696"
          mr="2"
        >{name} - 2 days ago</Text>
        <Rating
          rating={rating}
          color={color}
        />
      </Box>
      <Text fontSize="14px">{body} </Text>
    </Box>
  </Flex>
}
export default ReviewCard