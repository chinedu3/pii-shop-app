import { Flex, Image } from '@chakra-ui/react'
import React from 'react'

type ShareIconProps = {
  onClick: () => void
}

const ShareIcon = (props: ShareIconProps) => {
  const { onClick } = props;
  return (
    <Flex
      position="absolute"
      bottom="10px"
      right="10px"
      zIndex="5"
      bg="white"
      w="32px"
      h="32px"
      boxShadow="0px 0px 2.97872px rgba(0, 0, 0, 0.25)"
      borderRadius="6px"
      alignItems={"center"}
      justifyContent={"center"}
      onClick={(e) => {
        e.stopPropagation();
        onClick();
      }}
    >
      <Image src="https://res.cloudinary.com/pii/image/upload/v1647979180/share_gfquko.svg"
        alt="share icon"
        width="25px" />
    </Flex>
  )
}

export default ShareIcon