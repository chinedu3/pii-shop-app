export const PUBLIC_PATH = {
  SEARCH: "/search/:searchTerm",
  PRODUCT_INFO: "/product/:productId"
}