const variants = [
  {
    productId: "B09P8LCCX6",
    images: [],
    price: null,
    criteria: {
      Color: "Green Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8L2T4H",
    images: [],
    price: null,
    criteria: {
      Color: "Yellow Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8JBNKG",
    images: [],
    price: null,
    criteria: {
      Color: "Red Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8KVB8K",
    images: [],
    price: null,
    criteria: {
      Color: "Green Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8LX8H7",
    images: [
      "https://m.media-amazon.com/images/I/71vu0J7WbyL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61gEZOzEjOL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61nB5LymEcL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/81eHQxT2P5L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61fYPy2tJ-L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "Small",
    },
  },
  {
    productId: "B09P8L2D4P",
    images: [],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8KSSR3",
    images: [],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8LB8LY",
    images: [],
    price: null,
    criteria: {
      Color: "Green Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8KY9BB",
    images: [],
    price: null,
    criteria: {
      Color: "Yellow Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8L1TKR",
    images: [],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8KTZNM",
    images: [],
    price: null,
    criteria: {
      Color: "Green Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8LZ59Q",
    images: [
      "https://m.media-amazon.com/images/I/71EhF9-0fLL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61Ztv1CW5vL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61cNdls1U4L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/81JasIvsvuL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/71gD-aN7SwL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8L19ZY",
    images: [],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8M6SMF",
    images: [
      "https://m.media-amazon.com/images/I/81NaWAu01oL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61HXeLPMmKL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61ubQf4YYBL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/91CYlVFkXNL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/717SIjHS2LL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8LN3C3",
    images: [
      "https://m.media-amazon.com/images/I/71cRcyVhT9L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61JGxohZs0L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61TGN0IXuvL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/81DTzPJlyfL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/71AS4q0MmoL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Red Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8K9NNT",
    images: [],
    price: null,
    criteria: {
      Color: "Red Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8KGF6R",
    images: [],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "Large",
    },
  },
  {
    productId: "B09P8LJBZK",
    images: [],
    price: null,
    criteria: {
      Color: "Red Stripe",
      Size: "Small",
    },
  },
  {
    productId: "B09P8L7JS4",
    images: [],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8LHDGV",
    images: [],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8LFPR4",
    images: [
      "https://m.media-amazon.com/images/I/81J0D1KeY0L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M1-QEf7XL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61nJoR113cL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/911QD6y+76L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/71guwbSK0WL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Aqua Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8K3DC9",
    images: [],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8MD7D2",
    images: [
      "https://m.media-amazon.com/images/I/81PZiMmSuLL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61Vk1ilpqWL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/6171G4gD0HL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/91pIuxCeS6L._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/71I007gZtrL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Green Stripe",
      Size: "Large",
    },
  },
  {
    productId: "B09P8KDLZP",
    images: [],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "Small",
    },
  },
  {
    productId: "B09P8K8ZXD",
    images: [],
    price: null,
    criteria: {
      Color: "Black Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8L4TZV",
    images: [],
    price: null,
    criteria: {
      Color: "Yellow Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8KYKHY",
    images: [],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "Large",
    },
  },
  {
    productId: "B09P8KP8V8",
    images: [],
    price: null,
    criteria: {
      Color: "Aqua Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8LJJV4",
    images: [],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8JPQ2F",
    images: [],
    price: null,
    criteria: {
      Color: "Black Stripe",
      Size: "Small",
    },
  },
  {
    productId: "B09P8L59JL",
    images: [],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8KMWXQ",
    images: [],
    price: null,
    criteria: {
      Color: "Black Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8KMGLR",
    images: [],
    price: null,
    criteria: {
      Color: "Aqua Stripe",
      Size: "3X-Large",
    },
  },
  {
    productId: "B09P8KP2TS",
    images: [],
    price: null,
    criteria: {
      Color: "Red Stripe",
      Size: "Large",
    },
  },
  {
    productId: "B09P8L35LH",
    images: [],
    price: null,
    criteria: {
      Color: "Black Stripe",
      Size: "XX-Large",
    },
  },
  {
    productId: "B09P8LFDK6",
    images: [],
    price: null,
    criteria: {
      Color: "Aqua Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8LN4GN",
    images: [
      "https://m.media-amazon.com/images/I/71E4VkVMptL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61CGBV1oGUL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61x5MHbTHqL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/81hoaRFq9YL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/719ebPjZujL._AC_UL1500_.jpg",
      "https://m.media-amazon.com/images/I/61M0d3vJoiL._AC_UL1500_.jpg",
    ],
    price: null,
    criteria: {
      Color: "Yellow Stripe",
      Size: "X-Large",
    },
  },
  {
    productId: "B09P8KXYBQ",
    images: [],
    price: null,
    criteria: {
      Color: "Cobalt Stripe",
      Size: "Small",
    },
  },
  {
    productId: "B09P8KW2WM",
    images: [],
    price: null,
    criteria: {
      Color: "Purple Stripe",
      Size: "Medium",
    },
  },
  {
    productId: "B09P8L6B1X",
    images: [],
    price: null,
    criteria: {
      Color: "Olive Green Stripe",
      Size: "Large",
    },
  },
  {
    productId: "B09P8K125Z",
    images: [],
    price: null,
    criteria: {
      Color: "Black Stripe",
      Size: "Large",
    },
  },
];
const variants2 = [
  {
    productId: "B08N5KWB9H",
    images: [
      "https://m.media-amazon.com/images/I/71TPda7cwUL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/719BHaUQ46L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/91etkaXZjEL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/6155Fp7yaSL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61stcFwi0vL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71KpoUmtlqL._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "256GB",
      Color: "Silver",
    },
  },
  {
    productId: "B08N5LNQCX",
    images: [
      "https://m.media-amazon.com/images/I/71jG+e7roXL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/712v9WGWDBL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/91YEUMzK8cL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61uNK7su24L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61ChHwbxObL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71LG4S9ZB9L._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "256GB",
      Color: "Space Gray",
    },
  },
  {
    productId: "B08N5R2GQW",
    images: [
      "https://m.media-amazon.com/images/I/71TPda7cwUL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/719BHaUQ46L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/91etkaXZjEL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/6155Fp7yaSL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61stcFwi0vL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71KpoUmtlqL._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "512GB",
      Color: "Silver",
    },
  },
  {
    productId: "B08N5M9XBS",
    images: [
      "https://m.media-amazon.com/images/I/71vFKBpKakL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/81HZAfCGZ5L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/81PeNcC5W4L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61UVTIkDLuL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71hHMsqVzGL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71zFHzUBzuL._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "512GB",
      Color: "Gold",
    },
  },
  {
    productId: "B08N5M7S6K",
    images: [
      "https://m.media-amazon.com/images/I/71vFKBpKakL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/81HZAfCGZ5L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/81PeNcC5W4L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61UVTIkDLuL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71hHMsqVzGL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71zFHzUBzuL._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "256GB",
      Color: "Gold",
    },
  },
  {
    productId: "B08N5LFLC3",
    images: [
      "https://m.media-amazon.com/images/I/71jG+e7roXL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/712v9WGWDBL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/91YEUMzK8cL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61uNK7su24L._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/61ChHwbxObL._AC_SL1500_.jpg",
      "https://m.media-amazon.com/images/I/71LG4S9ZB9L._AC_SL1500_.jpg",
    ],
    price: null,
    criteria: {
      Capacity: "512GB",
      Color: "Space Gray",
    },
  },
];

const product2 = {
  variants: variants2,
  variantOptions: ["Capacity", "Color"],
};
const productDetails = {
  variants,
  variantOptions: ["Color", "Size"],
};

const selectedValues = {
  Capacity: "2256GB",
  Color: "Gold",
};

const createIdMapper = (productDetails) => {
  /**
   * This gives unique selection of variant options to display to the user
    {
    Capacity: Set(2) { '256GB', '512GB' },
    Color: Set(3) { 'Silver', 'Space Gray', 'Gold' }
    }

    complexity of O(n^2)
   */
  const idMapper = {};
  const variantIdMapper = {};
  for (const variant of productDetails.variants) {
    variantIdMapper[variant.productId] = variant;
    for (const [criteria, criteriaValue] of Object.entries(variant.criteria)) {
      idMapper[criteria] = new Set(idMapper[criteria]);
      idMapper[criteria].add(criteriaValue);
    }
  }

  return [idMapper, variantIdMapper];
};

const [idMapper, variantIdMapper] = createIdMapper(product2);

const getVariantId = (selection, productVariants) => {
  /**
   * This returns the exact variant based on the user selection or null if variant was not found
   *
   *  complexity of O(n^2)
   */
  let variantId = null;
  for (const variant of productVariants) {
    let foundSet = new Set();
    for (const [key, value] of Object.entries(variant.criteria)) {
      if (selection[key] == value) {
        foundSet.add(true);
      } else {
        foundSet.add(false);
      }
    }
    if (foundSet.has(false)) {
      continue;
    } else {
      variantId = variant.productId;
      break;
    }
  }
  if (!variantId) return null;
  return variantIdMapper[variantId];
};
