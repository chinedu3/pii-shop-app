export const productRequest = {
  SEARCH: "/products/search?search_text=:search&page=:page",
  PRODUCT_INFO: "/products/:productId?merchant=:merchantName",
  PRODUCT_REVIEW: "/products/:productId/reviews?merchant=:merchantName&page=:pageNo"
}
